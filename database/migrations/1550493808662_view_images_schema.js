/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const WebPerformance = use('App/Helpers/WebPerformance')

class ViewImagesSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_images'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      image: `${this.db_master}.ps_image`,
      image_lang: `${this.db_master}.ps_image_lang`,
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          i.id_image AS id,
          il.legend,
          i.position,
          i.cover,
          ${WebPerformance.isEnabled ? 'i.date_img_update,' : ''}
          i.id_product AS product_id
        FROM ${table.image} AS i
        INNER JOIN ${table.image_lang} AS il
          ON i.id_image = il.id_image
        ORDER BY i.cover DESC, i.position, i.id_image
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewImagesSchema
