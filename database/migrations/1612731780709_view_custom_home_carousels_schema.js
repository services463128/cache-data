/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewCustomHomeCarouselsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_custom_home_carousels'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.hasCustomHome()) {
      const table = {
        item: `${this.db_master}.ps_customhome_items`,
        content: `${this.db_master}.ps_customhome_carousels`,
        blocks: `${this.db_master}.ps_customhome_blocks`,
      }

      const store = Store.getCurrent()
      let fields = ''

      switch (store) {
        case Store.TIENDA_CLIC:
        case Store.VALYRIO:
          fields = 'c.slide_qty AS slideBy, i.link, b.show_in_store as show_in_store, b.active as active'
          break
        case Store.GALICIA:
          fields = 'c.slides_qty AS slideBy'
          break
        default:
          fields = 'i.link, b.show_in_store as show_in_store, b.active as active'
          break
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          c.item_id as id,
          i.title AS name,
          c.content_type AS type,
          i.position,
          c.content,
          ${fields}
        FROM ${table.item} AS i
        INNER JOIN ${table.content} AS c
        ON i.id = c.item_id
        INNER JOIN ${table.blocks} AS b
        ON i.block_id = b.id
      `)
    }
  }

  async down() {
    if (Store.hasCustomHome()) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewCustomHomeCarouselsSchema
