/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewPointsConversionsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_points_conversions'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  supportsAperpoints() {
    switch (Store.getCurrent()) {
      case Store.TIENDA_CLIC:
        return false
      default:
        return true
    }
  }

  async up() {
    if (this.supportsAperpoints()) {
      switch (Store.getCurrent()) {
        case Store.BBVA:
          var sql = `CREATE VIEW ${this.view_name} AS
          SELECT
            c.id,
            c.factor,
            c.category,
            c.description,
            c.isActive AS active
          FROM ${this.db_master}.ps_pointspayment_convertion AS c
        `;
          break;
        default:
          var sql = `CREATE VIEW ${this.view_name} AS
          SELECT
            c.id,
            c.factor,
            c.category,
            c.description,
            c.is_active AS active
          FROM ${this.db_master}.ps_aperpoint_convertion AS c
        `;
          break;
      }
      await Database.raw(sql);
    }
  }

  async down() {
    if (this.supportsAperpoints()) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewPointsConversionsSchema
