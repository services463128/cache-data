const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')
const WebPerformance = use('App/Helpers/WebPerformance')

class ViewProductsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_products'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  bbvaArView() {
    const table = {
      product: `${this.db_master}.ps_product`,
      product_lang: `${this.db_master}.ps_product_lang`,
      carrier: `${this.db_master}.ps_product_carrier`,
      stock: `${this.db_master}.ps_stock_available`,
      supplier: `${this.db_master}.ps_supplier`,
      images: `${this.db_master}.ps_image`,
    }

    return `
      SELECT
        p.id_product AS id,
        p.reference,
        pl.name,
        pl.description,
        pl.description_short,
        pl.link_rewrite AS slug,
        sa.quantity,
        p.outstanding,
        p.active,
        s.enable_pointspayment AS work_points,
        p.id_category_default AS category_id,
        sa.id_product_attribute AS product_attribute_id,
        sa.id_stock_available AS stock_id,
        p.id_supplier AS supplier_id,
        ${WebPerformance.isEnabled ? 'img.date_img_update,' : ''}
        p.date_add AS created_at,
        p.date_upd AS updated_at
      FROM ${table.product} AS p
      INNER JOIN ${table.product_lang} AS pl
        ON p.id_product = pl.id_product
      INNER JOIN ${table.stock} AS sa
        ON p.id_product = sa.id_product
      INNER JOIN ${table.images} AS img
        ON p.id_product = img.id_product AND img.cover = 1
      LEFT JOIN ${table.supplier} AS s
        ON p.id_supplier = s.id_supplier
      WHERE sa.id_product_attribute = 0
        AND p.id_product IN (SELECT id_product FROM ${table.carrier})
    `
  }

  itauArView() {
    const table = {
      product: `${this.db_master}.ps_product`,
      product_lang: `${this.db_master}.ps_product_lang`,
      carrier: `${this.db_master}.ps_product_carrier`,
      stock: `${this.db_master}.ps_stock_available`,
      supplier: `${this.db_master}.ps_supplier`,
      point: `${this.db_master}.ps_aperpoint_suppliers`,
    }

    return `
    SELECT
    p.id_product AS id,
    p.reference,
    pl.name,
    pl.description,
    pl.description_short,
    pl.link_rewrite AS slug,
    sa.quantity,
    p.outstanding,
    IFNULL(pt.active, 0) AS work_points,
    p.active,
    p.id_category_default AS category_id,
    sa.id_product_attribute AS product_attribute_id,
    sa.id_stock_available AS stock_id,
    p.id_supplier AS supplier_id,
    p.date_add AS created_at,
    p.date_upd AS updated_at
  FROM ${table.product} AS p
  INNER JOIN ${table.product_lang} AS pl
    ON p.id_product = pl.id_product
  INNER JOIN ${table.stock} AS sa
    ON p.id_product = sa.id_product
  LEFT JOIN ${table.supplier} AS s
    ON p.id_supplier = s.id_supplier
  LEFT JOIN ${table.point} AS pt
    ON s.id_supplier = pt.id_supplier
  WHERE sa.id_product_attribute = 0
    AND p.id_product IN (SELECT id_product FROM ${table.carrier})
    `
  }

  tclicView() {
    const table = {
      product: `${this.db_master}.ps_product`,
      product_lang: `${this.db_master}.ps_product_lang`,
      carrier: `${this.db_master}.ps_product_carrier`,
      stock: `${this.db_master}.ps_stock_available`,
      supplier: `${this.db_master}.ps_supplier`,
      images: `${this.db_master}.ps_image`,
    }

    return `
      SELECT
        p.id_product AS id,
        p.reference,
        pl.name,
        pl.description,
        pl.description_short,
        pl.link_rewrite AS slug,
        sa.quantity,
        p.outstanding,
        p.active,
        p.cuotas,
        p.cuotas_sin_interes,
        p.cuotas as installments_fixed,
        p.cuotas_sin_interes as installments_wo_interest,
        p.id_category_default AS category_id,
        sa.id_product_attribute AS product_attribute_id,
        sa.id_stock_available AS stock_id,
        p.id_supplier AS supplier_id,
        ${WebPerformance.isEnabled ? 'img.date_img_update,' : ''}
        p.date_add AS created_at,
        p.date_upd AS updated_at
      FROM ${table.product} AS p
      INNER JOIN ${table.product_lang} AS pl
        ON p.id_product = pl.id_product
      INNER JOIN ${table.stock} AS sa
        ON p.id_product = sa.id_product
      INNER JOIN ${table.images} AS img
        ON p.id_product = img.id_product AND img.cover = 1
      LEFT JOIN ${table.supplier} AS s
        ON p.id_supplier = s.id_supplier
      WHERE sa.id_product_attribute = 0
        AND p.id_product IN (SELECT id_product FROM ${table.carrier})
    `
  }

  default17View() {
    const table = {
      product: `${this.db_master}.ps_product`,
      product_lang: `${this.db_master}.ps_product_lang`,
      carrier: `${this.db_master}.ps_product_carrier`,
      stock: `${this.db_master}.ps_stock_available`,
      supplier: `${this.db_master}.ps_supplier`,
      point: `${this.db_master}.ps_aperpoint_suppliers`,
      tax_rules_group: `${this.db_master}.ps_tax_rules_group`,
      images: `${this.db_master}.ps_image`,
    }

    return `
      SELECT
        p.id_product AS id,
        p.reference,
        pl.name,
        pl.description,
        pl.description_short,
        pl.link_rewrite AS slug,
        pl.id_lang,
        sa.quantity,
        p.outstanding,
        IFNULL(pt.active, 0) AS work_points,
        p.active,
        p.installments_fixed,
        p.installments_wo_interest,
        p.id_category_default AS category_id,
        trg.name as tax_name,
        sa.id_product_attribute AS product_attribute_id,
        sa.id_stock_available AS stock_id,
        p.id_supplier AS supplier_id,
        p.date_add AS created_at,
        p.date_upd AS updated_at,
        img.date_img_update
      FROM ${table.product} AS p
      INNER JOIN ${table.product_lang} AS pl
        ON p.id_product = pl.id_product
      INNER JOIN ${table.stock} AS sa
        ON p.id_product = sa.id_product
      INNER JOIN ${table.images} AS img
        ON p.id_product = img.id_product AND img.cover = 1
      INNER JOIN ${table.tax_rules_group} AS trg
        ON p.id_tax_rules_group = trg.id_tax_rules_group
      LEFT JOIN ${table.supplier} AS s
        ON p.id_supplier = s.id_supplier
      LEFT JOIN ${table.point} AS pt
        ON s.id_supplier = pt.id_supplier
      WHERE sa.id_product_attribute = 0
        AND p.id_product IN (SELECT id_product FROM ${table.carrier})
    `
  }

  galiciaView() {
    const table = {
      product: `${this.db_master}.ps_product`,
      product_lang: `${this.db_master}.ps_product_lang`,
      carrier: `${this.db_master}.ps_product_carrier`,
      stock: `${this.db_master}.ps_stock_available`,
      supplier: `${this.db_master}.ps_supplier`,
      point: `${this.db_master}.ps_aperpoint_suppliers`,
      tax_rules_group: `${this.db_master}.ps_tax_rules_group`,
      images: `${this.db_master}.ps_image`,
    }

    return `
      SELECT
        p.id_product AS id,
        p.reference,
        pl.name,
        pl.description,
        pl.description_short,
        pl.link_rewrite AS slug,
        pl.id_lang,
        sa.quantity,
        p.outstanding,
        IFNULL(pt.active, 0) AS work_points,
        p.active,
        p.installments_fixed,
        p.installments_wo_interest,
        p.id_category_default AS category_id,
        trg.name as tax_name,
        sa.id_product_attribute AS product_attribute_id,
        sa.id_stock_available AS stock_id,
        p.id_supplier AS supplier_id,
        p.date_add AS created_at,
        p.date_upd AS updated_at
      FROM ${table.product} AS p
      INNER JOIN ${table.product_lang} AS pl
        ON p.id_product = pl.id_product
      INNER JOIN ${table.stock} AS sa
        ON p.id_product = sa.id_product
      INNER JOIN ${table.images} AS img
        ON p.id_product = img.id_product AND img.cover = 1
      INNER JOIN ${table.tax_rules_group} AS trg
        ON p.id_tax_rules_group = trg.id_tax_rules_group
      LEFT JOIN ${table.supplier} AS s
        ON p.id_supplier = s.id_supplier
      LEFT JOIN ${table.point} AS pt
        ON s.id_supplier = pt.id_supplier
      WHERE sa.id_product_attribute = 0
        AND p.id_product IN (SELECT id_product FROM ${table.carrier})
    `
  }

  viewSql() {
    switch (Store.getCurrent()) {
      case Store.TIENDA_CLIC:
        return this.tclicView()
      case Store.TIENDA_IUPP:
        return this.itauArView()
      case Store.BBVA:
        return this.bbvaArView()
      case Store.GALICIA:
        return this.galiciaView()
      default:
        return this.default17View()
    }
  }

  async up() {
    await Database.raw(`CREATE VIEW ${this.view_name} AS ${this.viewSql()}`)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewProductsSchema
