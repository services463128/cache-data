/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')

class ViewCategoriesProductsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_categories_products'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      categoryProduct: `${this.db_master}.ps_category_product`,
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          /*cp.id,*/
          cp.id_category AS category_id,
          cp.id_product AS product_id,
          cp.position
        FROM ${table.categoryProduct} AS cp
        ORDER BY cp.position ASC
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewCategoriesProductsSchema
