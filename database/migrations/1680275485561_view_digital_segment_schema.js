/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')

class ViewDigitalSegmentSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_digital_segment'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (this.db_master.includes('icbc') || this.db_master.includes('galicia')) {
      const table = {
        digital_segment: `${this.db_master}.ps_digital_segment`,
      }
      // Se trae todo lo de la tabla por si mas adelante se guarda datos como el limit
      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          *
        FROM ${table.digital_segment}
        ORDER BY id DESC
      `)
    }
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewDigitalSegmentSchema
