"use strict"

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use("Schema")
const Database = use("Database")

class ViewCarouselSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_carousels'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    let table = {
      item: `${this.db_master}.ps_home_carousel_item`,
      content: `${this.db_master}.ps_home_carousel_item_carousel`
    }

    await Database
      .raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          i.id,
          i.nombre AS name,
          i.tipo AS type,
          c.tipo_contenido AS type_content,
          c.contenido AS content,
          c.carousel_banner AS banner,
          c.carousel_banner_link AS banner_link,
          c.display AS display,
          c.autoplay AS autoplay,
          c.pagination AS pagination,
          i.orden AS position
        FROM ${table.item} AS i
        INNER JOIN ${table.content} AS c
          ON i.id = c.id_home_carousel_item
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewCarouselSchema

