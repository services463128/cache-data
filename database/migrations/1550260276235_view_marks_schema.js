/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewMarksSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_marks'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    await Database.raw(`CREATE VIEW ${this.view_name} AS ${this.viewSql()}`)
  }

  default17View() {
    const table = {
      marks: `${this.db_master}.ps_productsmarks`,
    }
    return `
      SELECT
      m.id_productsmarks AS id,
          m.name,
          m.description,
          m.position,
          m.type,
          m.priority
        FROM ${table.marks} AS m
      `
  }

  galiciaView() {
    const table = {
      marks: `${this.db_master}.ps_productsmarks`,
    }
    return `
      SELECT
      m.id_productsmarks AS id,
          m.name,
          m.description,
          m.position
        FROM ${table.marks} AS m
      `
  }

  viewSql() {
    switch (Store.getCurrent()) {
      case Store.GALICIA:
        return this.galiciaView()
      default:
        return this.default17View()
    }
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewMarksSchema
