const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewCarriersSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_carriers'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.getCurrent() === Store.BBVA) {
      const table = {
        carrier: `${this.db_master}.ps_carrier`,
        carrier_lang: `${this.db_master}.ps_carrier_lang`,
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          c.id_carrier AS id,
          c.name,
          cl.delay,
          c.is_free AS free
        FROM ${table.carrier} AS c
        INNER JOIN ${table.carrier_lang} AS cl
          ON cl.id_carrier = c.id_carrier
      `)
    }
  }

  async down() {
    if (Store.getCurrent() === Store.BBVA) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewCarriersSchema
