const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewAddressesSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_addresses'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.getCurrent() === Store.BBVA) {
      const table = {
        address: `${this.db_master}.ps_address`,
        country: `${this.db_master}.ps_country_lang`,
        state: `${this.db_master}.ps_state`,
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          a.id_address AS id,
          a.dni,
          a.street,
          a.number,
          a.floor,
          a.department_number,
          a.postcode,
          a.address1,
          a.address2,
          a.city,
          s.name AS state,
          cl.name AS country,
          a.id_customer AS customer_id,
          a.date_add AS created_at,
          a.date_upd AS updated_at
        FROM ${table.address} AS a
        INNER JOIN ${table.country} AS cl
          ON cl.id_country = a.id_country
        INNER JOIN ${table.state} AS s
          ON s.id_country = a.id_country AND s.id_state = a.id_state
        WHERE a.active = 1
      `)
    }
  }

  async down() {
    if (Store.getCurrent() === Store.BBVA) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewAddressesSchema
