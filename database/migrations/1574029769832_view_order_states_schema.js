const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewOrderStatesSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_order_states'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.getCurrent() === Store.BBVA) {
      const table = {
        order_state: `${this.db_master}.ps_order_state_lang`,
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          os.id_order_state AS id,
          os.name
        FROM ${table.order_state} AS os
      `)
    }
  }

  async down() {
    if (Store.getCurrent() === Store.BBVA) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewOrderStatesSchema
