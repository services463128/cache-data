const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewProductBestPricesSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_product_best_price'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.getCurrent() === Store.BBVA) {
      const table = {
        best_price: `${this.db_master}.ps_mejorprecio_conf`,
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          bp.id_mejor_precio AS id,
          bp.porcentaje AS percentage_retail,
          bp.tope_reintegro AS refund_stop_retail,
          bp.porcentaje_exclusive AS percentage_exclusive,
          bp.tope_reintegro_exclusive AS refund_stop_exclusive,
          bp.porcentaje_exclusive_payroll AS percentage_exclusive_payroll,
          bp.tope_reintegro_exclusive_payroll AS refund_stop_exclusive_payroll,
          bp.id_supplier AS supplier_id
        FROM ${table.best_price} AS bp
      `)
    }
  }

  async down() {
    if (Store.getCurrent() === Store.BBVA) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewProductBestPricesSchema
