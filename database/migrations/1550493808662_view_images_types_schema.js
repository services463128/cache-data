/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')

class ViewImagesTypeSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_images_type'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      image_type: `${this.db_master}.ps_image_type`,
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          i.id_image_type AS id,
          i.name,
          i.width,
          i.height,
          i.products,
          i.categories,
          i.manufacturers,
          i.suppliers,
          /*i.scenes, errorr*/
          i.stores
        FROM ${table.image_type} AS i
        ORDER BY i.id_image_type
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewImagesTypeSchema
