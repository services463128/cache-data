/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')

class ViewProductAttributeSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_products_attributes'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      ps_product_attribute: `${this.db_master}.ps_product_attribute`,
      ps_product: `${this.db_master}.ps_product`,
      ps_product_attribute_combination: `${this.db_master}.ps_product_attribute_combination`,
      ps_attribute: `${this.db_master}.ps_attribute`,
      ps_attribute_lang: `${this.db_master}.ps_attribute_lang`,
      ps_attribute_group_lang: `${this.db_master}.ps_attribute_group_lang`,
      ps_product_lang: `${this.db_master}.ps_product_lang`,
      ps_stock_available: `${this.db_master}.ps_stock_available`,
    }

    await Database.raw(`
        CREATE VIEW ${this.view_name} AS
        SELECT 
        ${table.ps_product}.id_product as id_product,
        ${table.ps_product_attribute_combination}.id_product_attribute as id_product_attribute,
        ${table.ps_product_lang}.name as detalle,
        ${table.ps_attribute_group_lang}.public_name,
        ${table.ps_attribute_lang}.name, 
        ${table.ps_stock_available}.quantity
        from ${table.ps_product_attribute}
        inner join ${table.ps_product} on ${table.ps_product}.id_product = ${table.ps_product_attribute}.id_product
        inner join ${table.ps_product_attribute_combination} on ${table.ps_product_attribute}.id_product_attribute = ${table.ps_product_attribute_combination}.id_product_attribute
        inner join ${table.ps_attribute} on ${table.ps_product_attribute_combination}.id_attribute = ${table.ps_attribute}.id_attribute 
        inner join ${table.ps_attribute_lang} on ${table.ps_attribute}.id_attribute = ${table.ps_attribute_lang}.id_attribute 
        inner join ${table.ps_attribute_group_lang} on ${table.ps_attribute}.id_attribute_group = ${table.ps_attribute_group_lang}.id_attribute_group 
        inner join ${table.ps_product_lang} on ${table.ps_product_attribute}.id_product = ${table.ps_product_lang}.id_product
        inner join ${table.ps_stock_available} on ${table.ps_stock_available}.id_product = ${table.ps_product_attribute}.id_product
        and ${table.ps_stock_available}.id_product_attribute = ${table.ps_product_attribute_combination}.id_product_attribute  `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewProductAttributeSchema
