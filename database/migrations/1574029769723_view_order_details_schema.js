const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewOrderDetailsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_order_details'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.getCurrent() === Store.BBVA) {
      const table = {
        order_details: `${this.db_master}.ps_order_detail`,
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          od.id_order_detail AS id,
          od.product_name,
          od.product_quantity,
          od.product_price AS price_base,
          od.unit_price_tax_incl AS price_final,
          od.reduction_percent,
          od.product_reference,
          od.id_order AS order_id,
          od.product_id,
          od.product_attribute_id
        FROM ${table.order_details} AS od
      `)
    }
  }

  async down() {
    if (Store.getCurrent() === Store.BBVA) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewOrderDetailsSchema
