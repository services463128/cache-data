const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewProductsMarksSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_marks_products'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  generateId() {
    switch (Store.getCurrent()) {
      case Store.TIENDA_CLIC:
        return '' // doesn't have one
      default:
        return 'mp.id,'
    }
  }

  async up() {
    const table = {
      marks_products: `${this.db_master}.ps_productsmarks_products`,
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          ${this.generateId()}
          mp.id_productsmarks AS mark_id,
          mp.id_product AS product_id
        FROM ${table.marks_products} AS mp
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewProductsMarksSchema
