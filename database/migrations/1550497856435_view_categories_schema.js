/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const WebPerformance = use('App/Helpers/WebPerformance')

class ViewCategoriesSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_categories'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      category: `${this.db_master}.ps_category`,
      category_lang: `${this.db_master}.ps_category_lang`,
    }

    let webPerfFields = ''

    if (WebPerformance.isEnabled) {
      webPerfFields = `
        c.date_img_update,
        c.date_img_two_update,
        c.date_img_thumb_update,
        c.date_img_thumbnail_update,`
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          cl.id_category AS id,
          cl.name,
          cl.description,
          cl.link_rewrite AS slug,
          c.id_parent,
          ${webPerfFields}
          c.active
        FROM ${table.category_lang} AS cl
        INNER JOIN ${table.category} AS c
        ON c.id_category = cl.id_category
        WHERE id_lang = 1
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewCategoriesSchema
