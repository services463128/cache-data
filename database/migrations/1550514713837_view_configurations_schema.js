/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')

class ViewConfigurationsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_configurations'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      configuration: `${this.db_master}.ps_configuration`,
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          c.id_configuration AS id,
          c.name,
          c.value
        FROM ${table.configuration} AS c
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewConfigurationsSchema
