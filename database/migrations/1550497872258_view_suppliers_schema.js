/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')

class ViewSuppliersSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_suppliers'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      supplier: `${this.db_master}.ps_supplier`,
      supplier_lang: `${this.db_master}.ps_supplier_lang`,
      address: `${this.db_master}.ps_address`,
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          s.id_supplier AS id,
          s.cuit,
          s.name,
          sl.description,
          s.email_post_venta AS email_post_sale,
          a.phone,
          CONCAT(a.address1, ' ', a.address2) AS address,
          s.active,
          s.date_add AS created_at,
          s.date_upd AS updated_at
        FROM ${table.supplier} AS s
        INNER JOIN ${table.supplier_lang} AS sl
          ON s.id_supplier = sl.id_supplier
        LEFT JOIN ${table.address} AS a
          ON a.id_supplier = s.id_supplier
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewSuppliersSchema
