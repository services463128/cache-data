const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewProductsPricesSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_products_prices'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  bancolombiaView() {
    const table = {
      product: `${this.db_master}.ps_product`,
      attribute: `${this.db_master}.ps_product_attribute`,
      price: `${this.db_master}.ps_specific_price`,
      tax: `${this.db_master}.ps_tax`,
      tax_rule: `${this.db_master}.ps_tax_rule`,
    }

    return `
      SELECT
        p.price AS base,
        IFNULL(sp.reduction, 0) AS reduction,
        sp.reduction_type,
        sp.from AS reduction_from,
        sp.to AS reduction_to,
        t.rate / 100 AS tax,
        p.installments_wo_interest,
        p.installments_fixed,
        p.id_product AS product_id
      FROM ${table.product} AS p
      LEFT JOIN ${table.attribute} AS pa
        ON p.id_product = pa.id_product AND p.reference = pa.reference
      LEFT JOIN ${table.price} AS sp
        ON p.id_product = sp.id_product AND
          ((DATE(NOW()) BETWEEN sp.from AND sp.to) OR
            (DATE_FORMAT(sp.from, '%Y-%m-%d') = '0000-00-00' AND DATE_FORMAT(sp.to, '%Y-%m-%d') = '0000-00-00') OR
            DATE(NOW()) <= sp.to)
          AND sp.id_product_attribute =
            CASE
              WHEN sp.id_product_attribute != 0
                THEN pa.id_product_attribute
                ELSE 0
            END
      INNER JOIN ${table.tax_rule} AS tr
        ON p.id_tax_rules_group = tr.id_tax_rules_group
      INNER JOIN ${table.tax} AS t
        ON tr.id_tax = t.id_tax
    `
  }

  bbvaArView() {
    const table = {
      product: `${this.db_master}.ps_product`,
      attribute: `${this.db_master}.ps_product_attribute`,
      price: `${this.db_master}.ps_specific_price`,
      tax: `${this.db_master}.ps_tax`,
      tax_rule: `${this.db_master}.ps_tax_rule`,
      coeficiente_cuotas: `${this.db_master}.ps_decidir_coeficiente_cuotas`,
    }

    return `
        SELECT
          p.price AS base,
          IFNULL(sp.reduction, 0) AS reduction,
          sp.reduction_type,
          sp.from AS reduction_from,
          sp.to AS reduction_to,
          t.rate / 100 AS tax,
          p.cuotas AS quota,
          p.cuotas_sin_interes AS quota_without_interest,
          p.id_product AS product_id,
          pdcc.coeficiente AS coeficient
        FROM ${table.product} AS p
        LEFT JOIN ${table.attribute} AS pa
          ON p.id_product = pa.id_product AND p.reference = pa.reference
        LEFT JOIN ${table.coeficiente_cuotas} AS pdcc
          ON pdcc.cuotas = p.cuotas
        LEFT JOIN ${table.price} AS sp
          ON p.id_product = sp.id_product
            AND (DATE(NOW()) BETWEEN sp.from AND sp.to OR '0000-00-00 00:00:00' BETWEEN sp.from AND sp.to)
            AND sp.id_product_attribute =
              CASE
                WHEN sp.id_product_attribute != 0
                  THEN pa.id_product_attribute
                  ELSE 0
              END
        INNER JOIN ${table.tax_rule} AS tr
          ON p.id_tax_rules_group = tr.id_tax_rules_group
        INNER JOIN ${table.tax} AS t
          ON tr.id_tax = t.id_tax
      `
  }

  itauArView() {
    const table = {
      product: `${this.db_master}.ps_product`,
      attribute: `${this.db_master}.ps_product_attribute`,
      price: `${this.db_master}.ps_specific_price`,
      tax: `${this.db_master}.ps_tax`,
      tax_rule: `${this.db_master}.ps_tax_rule`,
    }

    return `
    SELECT
          p.price AS base,
          IFNULL(sp.reduction, 0) AS reduction,
          sp.reduction_type,
          sp.from AS reduction_from,
          sp.to AS reduction_to,
          t.rate / 100 AS tax,
          p.installments_wo_interest,
          p.id_product AS product_id
        FROM ${table.product} AS p
        LEFT JOIN ${table.attribute} AS pa
          ON p.id_product = pa.id_product AND p.reference = pa.reference
        LEFT JOIN ${table.price} AS sp
          ON p.id_product = sp.id_product
            AND (DATE(NOW()) BETWEEN sp.from AND sp.to OR '0000-00-00 00:00:00' BETWEEN sp.from AND sp.to)
            AND sp.id_product_attribute =
              CASE
                WHEN sp.id_product_attribute != 0
                  THEN pa.id_product_attribute
                  ELSE 0
              END
        INNER JOIN ${table.tax_rule} AS tr
          ON p.id_tax_rules_group = tr.id_tax_rules_group
        INNER JOIN ${table.tax} AS t
          ON tr.id_tax = t.id_tax

      `
  }

  tclicView() {
    const table = {
      product: `${this.db_master}.ps_product`,
      attribute: `${this.db_master}.ps_product_attribute`,
      price: `${this.db_master}.ps_specific_price`,
      tax: `${this.db_master}.ps_tax`,
      tax_rule: `${this.db_master}.ps_tax_rule`,
    }

    return `
      SELECT
        p.price AS base,
        IFNULL(sp.reduction, 0) AS reduction,
        sp.reduction_type,
        sp.from AS reduction_from,
        sp.to AS reduction_to,
        t.rate / 100 AS tax,
        p.cuotas AS installments_fixed,
        p.cuotas_sin_interes As quota_without_interest,
        p.cuotas_sin_interes As installments_wo_interest,
        p.id_product AS product_id
      FROM ${table.product} AS p
      LEFT JOIN ${table.attribute} AS pa
        ON p.id_product = pa.id_product AND p.reference = pa.reference
      LEFT JOIN ${table.price} AS sp
        ON p.id_product = sp.id_product
          AND (DATE(NOW()) BETWEEN sp.from AND sp.to OR '0000-00-00 00:00:00' BETWEEN sp.from AND sp.to)
          AND sp.id_product_attribute =
            CASE
              WHEN sp.id_product_attribute != 0
                THEN pa.id_product_attribute
                ELSE 0
            END
      INNER JOIN ${table.tax_rule} AS tr
        ON p.id_tax_rules_group = tr.id_tax_rules_group
      INNER JOIN ${table.tax} AS t
        ON tr.id_tax = t.id_tax
    `
  }

  default17View() {
    const table = {
      product: `${this.db_master}.ps_product`,
      attribute: `${this.db_master}.ps_product_attribute`,
      price: `${this.db_master}.ps_specific_price`,
      tax: `${this.db_master}.ps_tax`,
      tax_rule: `${this.db_master}.ps_tax_rule`,
      suppliers_installments: `${this.db_master}.ps_suppliers_installments`,
      points_installments_supplier: `${this.db_master}.ps_points_installments_supplier`,
      range_installments: `${this.db_master}.ps_range_installments`,
    }

    return `
      SELECT
        p.price AS base,
        IFNULL(sp.reduction, 0) AS reduction,
        sp.reduction_type,
        sp.from AS reduction_from,
        sp.to AS reduction_to,
        t.rate / 100 AS tax,
        p.installments_wo_interest,
        p.installments_fixed,
        CASE WHEN si.active THEN ri.installments ELSE 1 END AS installments_points,
        p.id_product AS product_id
      FROM ${table.product} AS p
      LEFT JOIN ${table.attribute} AS pa
        ON p.id_product = pa.id_product AND p.reference = pa.reference
      LEFT JOIN ${table.price} AS sp
        ON p.id_product = sp.id_product
          AND (DATE(NOW()) BETWEEN sp.from AND sp.to OR '0000-00-00 00:00:00' BETWEEN sp.from AND sp.to)
          AND sp.id_product_attribute =
            CASE
              WHEN sp.id_product_attribute != 0
                THEN pa.id_product_attribute
                ELSE 0
            END
      INNER JOIN ${table.tax_rule} AS tr
        ON p.id_tax_rules_group = tr.id_tax_rules_group
      INNER JOIN ${table.tax} AS t
        ON tr.id_tax = t.id_tax
      LEFT JOIN ${table.suppliers_installments} AS si ON si.id_supplier = p.id_supplier
      LEFT JOIN ${table.range_installments} AS ri ON ri.installments IS NOT NULL
        AND ri.installments = (SELECT pri.installments
          FROM ${table.suppliers_installments} AS s
          LEFT JOIN ${table.points_installments_supplier} AS pis ON pis.id_suppliers_installments = s.id
          LEFT JOIN ${table.range_installments} AS pri ON pri.id = pis.id_range_installments AND pri.active = 1
          WHERE s.id_supplier = p.id_supplier
          ORDER BY pis.active DESC, pri.installments DESC
          LIMIT 1
        )
    `
  }

  viewSql() {
    switch (Store.getCurrent()) {
      case Store.BANCOLOMBIA:
        return this.bancolombiaView()
      case Store.BBVA:
        return this.bbvaArView()
      case Store.TIENDA_IUPP:
        return this.itauArView()
      case Store.TIENDA_CLIC:
        return this.tclicView()
      default:
        return this.default17View()
    }
  }

  async up() {
    await Database.raw(`CREATE VIEW ${this.view_name} AS ${this.viewSql()}`)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewProductsPricesSchema
