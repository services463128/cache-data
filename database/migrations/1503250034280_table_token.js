const Schema = use('Schema')
const Store = use('App/Helpers/Store')

class TokensSchema extends Schema {
  up() {
    if (Store.getCurrent() === Store.BBVA) {
      this.create('tokens', (table) => {
        table.increments()
        table.uuid('user_id').references('id').inTable('users')
        table.string('token', 255).notNullable().unique().index()
        table.string('type', 80).notNullable()
        table.boolean('is_revoked').defaultTo(false)
        table.timestamps()
      })
    }
  }

  down() {
    if (Store.getCurrent() === Store.BBVA) {
      this.drop('tokens')
    }
  }
}

module.exports = TokensSchema
