/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewMenuSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_menu'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  default16View() {
    const table = {
      category: `${this.db_master}.ps_category`,
      category_shop: `${this.db_master}.ps_category_shop`,
      category_lang: `${this.db_master}.ps_category_lang`,
      category_group: `${this.db_master}.ps_category_group`,
      configuration: `${this.db_master}.ps_configuration`,
    }

    return `
        SELECT c.id_category
        ,c.id_parent
        ,c.id_shop_default
        ,c.level_depth
        ,c.nleft
        ,c.nright
        ,c.active
        ,c.date_add
        ,c.date_upd
        ,c.position
        ,c.is_root_category
        ,cl.id_shop
        ,cl.id_lang
        ,cl.name
        ,cl.description
        ,cl.link_rewrite
        ,cl.meta_title
        ,cl.meta_keywords
        ,cl.meta_description
        ,CONCAT (
            CASE
                WHEN (
                        SELECT value
                        FROM ${table.configuration}
                        WHERE name = 'PS_SSL_ENABLED'
                        ) = 1
                    THEN CONCAT (
                            "https://"
                            ,(
                                SELECT value
                                FROM ${table.configuration}
                                WHERE name = 'PS_SHOP_DOMAIN_SSL'
                                )
                            )
                ELSE CONCAT (
                        "http://"
                        ,(
                            SELECT value
                            FROM ${table.configuration}
                            WHERE name = 'PS_SHOP_DOMAIN'
                            )
                        )
                END
            ,CONCAT (
                "/"
                ,c.id_category
                ,"-"
                ,cl.link_rewrite
                )
            ) AS link
        ,CASE
            WHEN EXISTS (
                    SELECT *
                    FROM ${table.configuration}
                    WHERE name = CONCAT (
                            "CAT_AVAILABLE_IN_MENU_"
                            ,c.id_category
                            )
                    )
                THEN 1
            ELSE 0
            END AS available
    FROM ${table.category} c
    INNER JOIN ${table.category_shop} category_shop ON (
            category_shop.id_category = c.id_category
            AND category_shop.id_shop = 1
            )
    LEFT JOIN ${table.category_lang} cl ON c.id_category = cl.id_category
        AND cl.id_shop = 1
    LEFT JOIN ${table.category_group} cg ON c.id_category = cg.id_category
    RIGHT JOIN ${table.category} c2 ON c2.id_category = 2
        AND c.nleft >= c2.nleft
        AND c.nright <= c2.nright
    WHERE 1
        AND id_lang = 1
        AND cg.id_group IN (1)
    GROUP BY c.id_category
    ORDER BY c.level_depth ASC
        ,category_shop.position ASC;
      `
  }

  default17View() {
    const table = {
      zmenu: `${this.db_master}.ps_zmenu`,
      zmenu_lang: `${this.db_master}.ps_zmenu_lang`,
      zdropdown: `${this.db_master}.ps_zdropdown`,
      zdropdown_lang: `${this.db_master}.ps_zdropdown_lang`,
    }

    return `
        SELECT
          m.id_zmenu as 'm.id_zmenu',
          m.id_shop,
          m.active,
          m.position,
          m.drop_column,
          ml.id_zmenu as 'ml.id_zmenu',
          ml.name,
          ml.link,
          d.categories,
          d.content_type
        FROM ${table.zmenu} AS m
        LEFT JOIN ${table.zmenu_lang} AS ml
          ON m.id_zmenu = ml.id_zmenu
        LEFT JOIN ${table.zdropdown} AS d
          ON d.id_zmenu = m.id_zmenu AND content_type = 'category'
        LEFT JOIN ${table.zdropdown_lang} AS dl
          ON d.id_zdropdown = dl.id_zdropdown
        WHERE m.id_shop = 1
        AND ml.id_lang = 1
        AND m.active = 1
        GROUP BY m.id_zmenu, d.id_zdropdown
        ORDER BY m.position, d.position;
      `
  }

  viewSql() {
    if (Store.isCurrentStore16()) {
      return this.default16View()
    }
    return this.default17View()
  }

  async up() {
    await Database.raw(`CREATE VIEW ${this.view_name} AS ${this.viewSql()}`)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewMenuSchema
