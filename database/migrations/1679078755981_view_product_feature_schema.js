/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')

class ViewProductFeatureSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_products_features'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      product: `${this.db_master}.ps_feature_product`,
      feature_lang: `${this.db_master}.ps_feature_lang`,
      feature_value_lang: `${this.db_master}.ps_feature_value_lang`,
    }

    await Database.raw(`
        CREATE VIEW ${this.view_name} AS
        SELECT
        fl.name as feature_name,
        fvl.value as feature_value,
        fp.id_product,
        fp.id_feature,
        fp.id_feature_value
        FROM ${table.product} AS fp
        INNER JOIN ${table.feature_lang} AS fl
          ON fp.id_feature = fl.id_feature
        INNER JOIN ${table.feature_value_lang} AS fvl
          ON fp.id_feature_value = fvl.id_feature_value
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewProductFeatureSchema
