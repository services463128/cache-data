const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewPointsToolsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_points_tools'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  supportsAperpoints() {
    switch (Store.getCurrent()) {
      case Store.TIENDA_CLIC:
        return false
      default:
        return true
    }
  }

  async up() {
    if (this.supportsAperpoints()) {
      switch (Store.getCurrent()) {
        case Store.BBVA:
          var table = {
            points_tools: `${this.db_master}.ps_pointspayment_tools`,
          }
          break;
        default:
          var table = {
            points_tools: `${this.db_master}.ps_aperpoint_tools`,
          }
          break;
      }
      await Database.raw(`CREATE VIEW ${this.view_name} AS
          SELECT *
          FROM ${table.points_tools}
        `)
    }
  }

  async down() {
    if (this.supportsAperpoints()) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewPointsToolsSchema
