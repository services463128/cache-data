const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewOrdersSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_orders'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.getCurrent() === Store.BBVA) {
      const table = {
        order: `${this.db_master}.ps_orders`,
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          o.id_order AS id,
          o.reference,
          o.total_paid,
          o.total_products_wt AS total_products,
          o.total_shipping,
          o.current_state AS state_id,
          o.id_currency AS currency_id,
          o.id_cart AS cart_id,
          o.id_carrier AS carrier_id,
          o.id_customer AS customer_id,
          o.id_address_delivery AS address_delivery_id,
          o.id_address_invoice AS address_invoice_id,
          o.date_add AS created_at,
          o.date_upd AS updated_at
        FROM ${table.order} AS o
      `)
    }
  }

  async down() {
    if (Store.getCurrent() === Store.BBVA) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewOrdersSchema
