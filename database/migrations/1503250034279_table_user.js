const Schema = use('Schema')
const Store = use('App/Helpers/Store')

class UserSchema extends Schema {
  up() {
    if (Store.getCurrent() === Store.BBVA) {
      this.create('users', (table) => {
        table.uuid('id').primary()
        table.string('username', 80).notNullable().unique()
        table.string('email', 254).notNullable().unique()
        table.string('password', 60).notNullable()
        table.string('secret', 60).nullable()
        table.timestamps()
      })
    }
  }

  down() {
    if (Store.getCurrent() === Store.BBVA) {
      this.drop('users')
    }
  }
}

module.exports = UserSchema
