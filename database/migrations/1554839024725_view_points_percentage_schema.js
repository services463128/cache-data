/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewPointsPercentageSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_points_percentage'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  supportsAperpoints() {
    switch (Store.getCurrent()) {
      case Store.TIENDA_CLIC:
        return false
      default:
        return true
    }
  }

  bbvaArView() {
    const table = {
      percentage: `${this.db_master}.ps_pointspayment_porcentajes_disponibles`,
    }

    return `
        SELECT
          p.id,
          p.porcentaje / 100 AS percentage,
          p.isActive AS active
        FROM ${table.percentage} AS p
      `
  }

  default17View() {
    const table = {
      percentage: `${this.db_master}.ps_aperpoint_suppliers`,
    }

    return `
        SELECT
          id,
          id_supplier,
          name,
          active,
          available_points
        FROM ${table.percentage}
      `
  }

  viewSql() {
    switch (Store.getCurrent()) {
      case Store.BBVA:
        return this.bbvaArView()
      default:
        return this.default17View()
    }
  }

  async up() {
    if (this.supportsAperpoints()) {
      await Database.raw(`CREATE VIEW ${this.view_name} AS ${this.viewSql()}`)
    }
  }

  async down() {
    if (this.supportsAperpoints()) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewPointsPercentageSchema
