/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class CategoriesCriterionSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_categories_criterion'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  fields() {
    if (Store.isCurrentStore16()) {
      return `
        c.criterio AS criterion,
        c.orden AS 'order',
      `
    }
    return `
      c.criteria AS criterion,
      c.order AS 'order',
    `
  }

  async up() {
    const table = {
      category: `${this.db_master}.ps_category_order_criteria`,
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          c.id,
          ${this.fields()}
          c.id_category AS category_id
        FROM ${table.category} AS c
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = CategoriesCriterionSchema
