const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewCustomersSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_customers'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.getCurrent() === Store.BBVA) {
      const table = {
        customer: `${this.db_master}.ps_customer`,
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          c.id_customer AS id,
          c.firstname,
          c.lastname,
          c.email,
          c.active,
          c.date_add AS created_at,
          c.date_upd AS updated_at
        FROM ${table.customer} AS c
      `)
    }
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = ViewCustomersSchema
