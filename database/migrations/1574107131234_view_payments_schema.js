const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')
const Store = use('App/Helpers/Store')

class ViewPaymentsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_payments'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    if (Store.getCurrent() === Store.BBVA) {
      const table = {
        transaction: `${this.db_master}.ps_decidir_transactions`,
        card: `${this.db_master}.ps_decidir_tarjetas`,
      }

      await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          d.id,
          d.nro_tarjeta AS first_digits,
          d.last_four_digits AS last_digits,
          IFNULL(d.monto_con_interes, 0) AS amount_with_financing,
          IFNULL(d.monto_sin_interes, 0) AS amount_without_financing,
          IFNULL(d.costo_financiero, 0) AS financing,
          d.cuotas AS quotas,
          t.nombre_tarjeta AS card_brand,
          d.estado AS state,
          d.cupon AS coupon,
          d.id_supplier AS supplier_id,
          d.id_order AS cart_id,
          d.nro_ticket AS payment_id,
          d.nro_operacion AS site_transaction_id,
          d.date_add AS created_at
        FROM ${table.transaction} AS d
        INNER JOIN ${table.card} AS t
          ON t.id_tarjeta_decidir = d.payment_method_id
      `)
    }
  }

  async down() {
    if (Store.getCurrent() === Store.BBVA) {
      await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
    }
  }
}

module.exports = ViewPaymentsSchema
