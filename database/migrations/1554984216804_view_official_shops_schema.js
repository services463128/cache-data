/** @type {import('@adonisjs/lucid/src/Schema')} */
const Env = use('Env')
const Schema = use('Schema')
const Database = use('Database')

class OfficialShopsSchema extends Schema {
  constructor() {
    super(Database)

    this.view_name = 'v_official_shops'
    this.db_master = Env.get('DB_STORE_MASTER', null)
  }

  async up() {
    const table = {
      shops: `${this.db_master}.ps_tiendas_oficiales`,
    }

    await Database.raw(`CREATE VIEW ${this.view_name} AS
        SELECT
          s.id_tienda_oficial AS id,
          s.nombre AS name,
          s.link,
          s.orden AS position,
          s.active,
          s.id_main AS main_id,
          s.id_tiendas_oficiales_padre AS parent_id,
          s.id_category AS category_id
        FROM ${table.shops} AS s
        WHERE active = 1
        ORDER BY position ASC
      `)
  }

  async down() {
    await Database.raw(`DROP VIEW IF EXISTS ${this.view_name}`)
  }
}

module.exports = OfficialShopsSchema
