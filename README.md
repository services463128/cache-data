# Aper Payment

This is an Adonisjs development used to supply information to the homepage of every Store.

It is consumed depending on the configuration set in module CustomHome, ZoneoneMegaMenu and other implementations.

This application loads both menu, the mobile menu and the desktop one. Both use the same routine to populate, but two calls are recivied, since each component in front implementations are independent.

Currently supported implementations:

- Valyrio
- Galicia
- Bancolombia
- TClic
- ItauAr

## Installation

### Requirements

- Node 18 or NVM

### Running without Docker

1. Clone repository

```bash
  git clone git@gitlab.com:apernet/stores/services/aperdata.git
```

2. Install dependencies

```bash
  yarn install
```

3. Create an configure .env file

```bash
  cp .env.example .env
```

- Edit .env file

```
HOST=127.0.0.1
PORT=3333
NODE_ENV=development

STORE=valyrio // valyrio, galicia, tclic, supervielle, bancolombia, etc ...

APP_NAME=AdonisJs
APP_URL=http://${HOST}:${PORT}

CACHE_VIEWS=false

APP_KEY=

DB_CONNECTION=sqlite
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER=root
DB_PASSWORD=
DB_DATABASE=adonis
DB_DEBUG=false
DB_STORE_MASTER=

REDIS_CONNECTION=local

REDIS_LOCAL_HOST=127.0.0.1
REDIS_LOCAL_PORT=6379
REDIS_LOCAL_PASSWORD=null
REDIS_LOCAL_DATABASE=0

REDIS_CLUSTER_HOSTS=127.0.0.1,127.0.0.2
REDIS_CLUSTER_PORT=6379
REDIS_CLUSTER_PASSWORD=null

HASH_DRIVER=bcrypt

CACHE_STORE=object

CORS_ORIGIN=

// Domain where D3 bucket is stored - for local instances use http://localhost:4566
S3_DOMAIN=
```

4. Run application

```bash
  yarn run start
```

### Running with Docker

1. Clone repository

```bash
  git clone git@gitlab.com:apernet/stores/services/aperdata.git
```

2. Create an configure .env file

```bash
  cp .env.example .env
```

- Edit .env file

```
HOST=127.0.0.1
PORT=3333
NODE_ENV=development

STORE=valyrio // valyrio, galicia, tclic, supervielle, bancolombia, etc ...

APP_NAME=AdonisJs
APP_URL=http://${HOST}:${PORT}

CACHE_VIEWS=false

APP_KEY=

DB_CONNECTION=sqlite
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER=root
DB_PASSWORD=
DB_DATABASE=adonis
DB_DEBUG=false
DB_STORE_MASTER=

REDIS_CONNECTION=local

REDIS_LOCAL_HOST=127.0.0.1
REDIS_LOCAL_PORT=6379
REDIS_LOCAL_PASSWORD=null
REDIS_LOCAL_DATABASE=0

REDIS_CLUSTER_HOSTS=127.0.0.1,127.0.0.2
REDIS_CLUSTER_PORT=6379
REDIS_CLUSTER_PASSWORD=null

HASH_DRIVER=bcrypt

CACHE_STORE=object

CORS_ORIGIN=

// Domain where D3 bucket is stored - for local instances use http://localhost:4566
S3_DOMAIN=
```

3. Run application

```bash
docker build -t aperdata-local .
docker run -t -p 3333:3333  --name aperdata-local  -v "$(pwd)"/.env:/app/.env  aperdata-local
```

### Store configuration

This services support multiple stores. When you run the services or run the db migration you need to define the STORE env variable. This variable is managed by the Store.js helper which is use in serveral part of the code to take into account the differencies on each implementation. Some differences are due to 1.6 vs 1.7 not been fully compatible and otherones are because tables or column don't use the same name across stores.

### Web Performance

For web perf. image URLs need to be build in a diferrente way. Currently the services support both, and automatically which between them depending if S3_DOMAIN is defined. This variable also need to be defined to run the migration scripts.

### Using UAT or Prod database

First you need to be logined in on the aws cli, using `aws configure sso` or `aws sso login --profile <profile>`, then you need to create the tunnel:

```bash
aws ssm start-session \
--profile <profile> \
--target <id or a instances, for ex: i-078f17a07400e82ca> \
--document-name AWS-StartPortForwardingSessionToRemoteHost \
--parameters host="valyrio-prod-a.chfcurzsyqti.us-east-1.rds.amazonaws.com",portNumber="3306",localPortNumber="63306"
```

This command forward the port 3306 of the database through the machine into our localhost port 63306.

## Routes and tips

- http://127.0.0.1:3333/cache/clear -> Clear cache
- http://127.0.0.1:3333/menu -> Returns menu
- Local Enviroment

  Go to Module Manager and look up for "CustomHome" & "ZoneoneMegaMenu", configure any of them and set "http://127.0.0.1:3333" value to domain, and save.
