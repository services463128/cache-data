const Database = use('Database')
const User = use('App/Managers/UserManager')
const { Command } = require('@adonisjs/ace')

class OauthPersonalApiToken extends Command {
  static get signature() {
    return 'oauth:personal-api-token'
  }

  static get description() {
    return 'Create a client for using Personal API Token'
  }

  async handle() {
    try {
      const username = await this.ask('Username:')
      const email = await this.ask('Email:')
      const password = await this.secure('Password:')
      // const password_comformation = await this.secure('Password confirmation:')
      const createSecret = true

      const data = {
        username,
        email,
        password,
      }

      const { user, secret } = await User.create(data, createSecret)

      this.completed('ID', user.id)
      this.completed('SECRET', secret)
    } catch ({ message }) {
      this.error(message)
    }
    Database.close()
  }
}

module.exports = OauthPersonalApiToken
