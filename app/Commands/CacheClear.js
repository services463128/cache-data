const Cache = use('Cache')

const { Command } = require('@adonisjs/ace')

class CacheClear extends Command {
  static get signature() {
    return 'cache:clear'
  }

  static get description() {
    return 'Flush the application cache'
  }

  async handle() {
    try {
      await Cache.flush()
      this.success(`${this.icon('success')} Application cache cleared!`)
    } catch (e) {
      console.error(`${this.icon('error')}`, e)
    }
  }
}

module.exports = CacheClear
