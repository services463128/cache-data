const Order = use('App/Managers/OrderManager')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with caches
 */
class OrderController {
  async show({ params: { id }, response }) {
    const data = await Order.find(id)

    return response.status(200).json(data)
  }
}

module.exports = OrderController
