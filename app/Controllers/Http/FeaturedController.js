const Cache = use('App/Helpers/Cache')
const featured = use('App/Managers/FeaturedManager')

const CustomHomeFeaturedCache = new Cache('redis')

const Env = use('Env')

class FeaturedController {
  async show({ params: { id, limit }, response }) {
    const lim = limit || Env.get('DEFAULT_LIMIT')

    const cacheKey = `customhomefeatured_${id}`

    const data = await CustomHomeFeaturedCache.getOrFetch(cacheKey, () => featured.products(id, lim))

    return response.status(200).json(data)
  }
}

module.exports = FeaturedController
