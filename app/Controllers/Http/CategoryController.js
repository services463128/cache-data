const Cache = use('App/Helpers/Cache')
const Category = use('App/Models/Category')

const CategoryControllerCache = new Cache('redis')

class CategoryController {
  async show({ params: { id }, response }) {
    const cacheKey = `categories_${id}`

    const data = await CategoryControllerCache.getOrFetch(cacheKey, () => Category.find(id))

    return response.status(200).json(data)
  }
}

module.exports = CategoryController
