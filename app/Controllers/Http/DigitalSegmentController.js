const Category = use('App/Managers/CategoryManager')
const Database = use('Database')
const Function = use('App/Helpers/Function')

const DEFAULT_PAGE_NUMBER = 1
const DEFAULT_PAGE_SIZE = 10

class DigitalSegmentController {
  async all({ response }) {
    const categories = await DigitalSegmentController.getCategories()
    return response.status(200).json(categories)
  }

  static async getCategories() {
    return (await Database.query().select('id_categories').from('v_digital_segment').last(1)).id_categories.split(',')
  }

  async show({ params, response, request }) {
    let categoryProducts
    const parameters = params
    parameters.page = request.get().p ? request.get().p : DEFAULT_PAGE_NUMBER
    parameters.size = request.get().s ? request.get().s : DEFAULT_PAGE_SIZE
    parameters.product = request.get().id_product ? request.get().id_product : ''
    parameters.from = Function.convertDate(request.get().fecha_actualizacion ? request.get().fecha_actualizacion : '')

    try {
      const dsCategories = await DigitalSegmentController.getCategories()

      if (dsCategories.includes(parameters.id)) {
        categoryProducts = await Category.products(parameters)
      } else {
        return response.status(404).json({ error: 'Categoria no listada en BackOffice' })
      }

      return categoryProducts
    } catch (error) {
      console.log(error)
      return response.status(500).json({ error: 'Error al procesar la solicitud' })
    }
  }
}

module.exports = DigitalSegmentController
