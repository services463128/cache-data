const InvalidGrantTypeException = use('App/Exceptions/InvalidGrantTypeException')
const { DateTime } = require('luxon')

class TokenController {
  async issueToken({ auth, request, response }) {
    const token = {}

    const { grant_type: grantType } = request.post()

    switch (grantType) {
      case 'client_credentials':
        token.data = await this._clientCredentialsGrant({ auth, request })
        token.status = 200
        break
      case 'password':
        token.data = await this.passwordGrant({ auth, request })
        token.status = 200
        break
      case 'refresh_token':
        token.data = await this._refreshTokenGrant({ auth, request })
        token.status = 200
        break
      default:
        throw new InvalidGrantTypeException()
    }

    return response.status(token.status).json(token.data)
  }

  async _clientCredentialsGrant({ auth, request }) {
    const { client_id: clientId, client_secret: clientSecret } = request.post()

    const token = await auth.withRefreshToken().attempt(clientId, clientSecret)

    return this._tokenResponse(token)
  }

  async _passwordGrant({ auth, request }) {
    const { username, password } = request.post()

    const token = await auth.withRefreshToken().attempt(username, password)

    return this._tokenResponse(token)
  }

  async _refreshTokenGrant({ auth, request }) {
    const { refresh_token: refreshToken } = request.post()

    const token = await auth.withRefreshToken().generateForRefreshToken(refreshToken)

    return this._tokenResponse(token)
  }

  _tokenResponse(token) {
    const claim = JSON.parse(Buffer.from(token.token.split('.')[1], 'base64').toString())

    return {
      access_token: token.token,
      token_type: token.type,
      expires_in: claim.exp - DateTime.now().toUnixInteger(),
      refresh_token: token.refreshToken,
    }
  }
}

module.exports = TokenController
