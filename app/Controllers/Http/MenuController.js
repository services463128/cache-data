const Menu = use('App/Managers/MenuManager')
const Cache = use('App/Helpers/Cache')

const MenuControllerCache = new Cache('redis')

class MenuController {
  async index({ response }) {
    const cacheKey = `menu`

    const data = await MenuControllerCache.getOrFetch(cacheKey, () => Menu.generate())

    return response.status(200).json(data)
  }

  async items({ response }) {
    const cacheKey = 'menu-items'

    const data = await MenuControllerCache.getOrFetch(cacheKey, () => Menu.generateItems())

    return response.status(200).json(data)
  }

  async categories({ response }) {
    const cacheKey = 'menu-categories'

    const data = await MenuControllerCache.getOrFetch(cacheKey, () => Menu.generateCategories())

    return response.status(200).json(data)
  }
}

module.exports = MenuController
