const Cache = use('Cache')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with caches
 */
class CacheController {
  /**
   * Show a list of all caches.
   * GET caches
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async flush({ response }) {
    const data = {
      status: 'error',
      description: 'Sorry, an unexpected error has occurred',
    }

    try {
      await Cache.flush()

      data.status = 'OK'
      data.description = 'Application cache cleared!'
    } catch (e) {
      console.error(e)
      data.exception = e.toString()
    }

    return response.status(200).json(data)
  }
}

module.exports = CacheController
