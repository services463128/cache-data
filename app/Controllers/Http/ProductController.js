const Product = use('App/Managers/ProductManager')
const Cache = use('App/Helpers/Cache')

const ProductControllerCache = new Cache('redis')

class ProductController {
  async show({ params: { id }, response }) {
    const cacheKey = `products_${id}`
    const ttl = 15 * 60 // min

    const data = await ProductControllerCache.getOrFetch(cacheKey, () => Product.find(id), ttl)

    return response.status(200).json(data)
  }
}

module.exports = ProductController
