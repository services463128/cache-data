const Cache = use('App/Helpers/Cache')
const Carousel = use('App/Managers/CarouselManager')

const CarouselControllerCache = new Cache('redis')

/**
 * Resourceful controller for interacting with caches
 */
class CarouselController {
  async show({ params: { id, type }, response }) {
    const cacheKey = `carousel_${type}_${id}`
    let data

    switch (type) {
      case 'category':
        data = await CarouselControllerCache.getOrFetch(cacheKey, () => Carousel.products(id))
        break
      case 'categories':
        data = await CarouselControllerCache.getOrFetch(cacheKey, () => Carousel.categories(id))
        break
      case 'product':
      case 'products':
        data = await CarouselControllerCache.getOrFetch(cacheKey, () => Carousel.products(id))
        break
      case 'related':
        data = await CarouselControllerCache.getOrFetch(cacheKey, () => Carousel.productsRelated(id))
        break
      case 'images':
        data = await CarouselControllerCache.getOrFetch(cacheKey, () => Carousel.images(id))
        break
      case 'shops':
        data = await CarouselControllerCache.getOrFetch(cacheKey, () => Carousel.shops(id))
        break
      case 'subcategories':
        data = await CarouselControllerCache.getOrFetch(cacheKey, () => Carousel.subcategories(id))
        break
      default:
        throw new Error(`Unexpected type ${type}`)
    }

    return response.status(200).json(data)
  }

  async getEndpoints({ response }) {
    const carousels = await Carousel.all_active_carousels()
    const endpoints = []

    /*
      "type": "Category",
      "name": "Televisores"
      "uri": ""external/carousel/categories/3"
    */

    for (const carousel of carousels) {
      const { id, name, type, show_in_store: showInStore } = carousel

      const carouselType = type === 'Product' ? 'Products' : type

      endpoints.push({
        name,
        type: carouselType,
        show_in_store: Boolean(Number(showInStore)),
        endpoint: `external/carousel/${carouselType.toLowerCase()}/${id}`,
      })
    }

    return response.status(200).json(JSON.stringify(endpoints))
  }
}

module.exports = CarouselController
