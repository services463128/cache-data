const InvalidContentTypeException = use('App/Exceptions/InvalidContentTypeException')

class CheckContentType {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request }, next) {
    if (!request.is(['application/x-www-form-urlencoded'])) {
      throw new InvalidContentTypeException()
    }

    // call next to advance the request
    await next()
  }
}

module.exports = CheckContentType
