const { LogicalException } = require('@adonisjs/generic-exceptions')

class CustomException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle(error, { response }) {
    response.status(error.status).send(error.message)
  }
}

module.exports = CustomException
