const BaseExceptionHandler = use('BaseExceptionHandler')
const { LogicalException } = require('@adonisjs/generic-exceptions')

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options
   *
   * @return {void}
   */
  async handle(error, options) {
    const { response } = options

    if (!(error instanceof LogicalException)) {
      const json = {
        status: error.status,
        code: error.code,
        message: error.message,
        errors: error.errors,
      }
      if (use('Env').get('NODE_ENV') !== 'production') {
        json.traces = error.stack
        return response.status(error.status).json(json)
      }

      return response.status(error.status).send()
    }
    return super.handle(error, options)
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report(error) {
    if (error.status === 500) {
      console.error(error)
    }
  }
}

module.exports = ExceptionHandler
