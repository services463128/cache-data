const { LogicalException } = require('@adonisjs/generic-exceptions')

class CarosuelNotExistException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle(error, { response }) {
    response.status(404).send('Not Found')
  }
}

module.exports = CarosuelNotExistException
