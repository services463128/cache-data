const { LogicalException } = require('@adonisjs/generic-exceptions')

const message = 'The contet-type is invalid. https://tools.ietf.org/html/rfc6749#section-4.4.2'
const status = 400
const code = 'E_INVALID_CONTENT_TYPE'

class InvalidContentTypeException extends LogicalException {
  constructor() {
    super(message, status, code)
  }
}

module.exports = InvalidContentTypeException
