const { LogicalException } = require('@adonisjs/generic-exceptions')

const message = 'The grant_tpye is not defined or invalid.'
const status = 400
const code = 'E_INVALID_JWT_GRANT_TYPE'

class InvalidGrantTypeException extends LogicalException {
  constructor() {
    super(message, status, code)
  }
}

module.exports = InvalidGrantTypeException
