const Database = use('Database')

class ProductRepository {
  constructor() {
    this.table = {
      product: 'ps_product',
      product_lang: 'ps_product_lang',
      stock: 'ps_stock_available',
    }
  }

  async find() {
    const data = Database.select(
      'p.id_product AS id',
      'p.reference',
      'pl.name',
      'pl.description',
      'pl.description_short',
      'p.price',
      'p.cuotas AS quotas',
      'pl.link_rewrite',
      'sa.quantity',
      'p.active',
      'p.id_category_default AS category_id',
      'sa.id_product_attribute AS product_attribute_id',
      'sa.id_stock_available AS stock_id',
      'p.id_supplier AS supplier_id',
      'date_add AS created_at',
      'date_upd AS updated_at'
    )
      // .select('*')
      .from(`${this.table.product} AS p`)
      .innerJoin(`${this.table.product_lang} AS pl`, 'p.id_product', 'pl.id_product')
      .innerJoin(`${this.table.stock} AS sa`, 'p.id_product', 'sa.id_product')
      .where('sa.id_product_attribute1', 0)
      .first()

    return data
  }
}

module.exports = ProductRepository
