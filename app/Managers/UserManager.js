const User = use('App/Models/User')
const Encryption = use('Encryption')
const { v4: uuidv4 } = require('uuid')

class UserManager {
  static async create(data, withSecret = false) {
    let secret = null

    if (withSecret) {
      secret = Encryption.encrypt(uuidv4())
        .replace(/[^A-Za-z0-9]+/g, '')
        .substr(0, 59)
    }

    const user = await User.create({ ...data, secret })

    return { user, secret }
  }
}

module.exports = UserManager
