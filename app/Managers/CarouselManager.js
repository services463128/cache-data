/* eslint-disable no-param-reassign */
const Env = use('Env')
const Cache = use('App/Helpers/Cache')
const Category = use('App/Models/Category')
const CategoryCriteron = use('App/Models/CategoryCriterion')
const Carousel = use('App/Models/Carousel')
const CustomHomeCarousel = use('App/Models/CustomHomeCarousel')
const Banner = use('App/Models/CarouselBanner')
const Product = use('App/Models/Product')
const OfficialShop = use('App/Models/OfficialShop')
const CarosuelNotExistException = use('App/Exceptions/CarosuelNotExistException')
const Generate = use('App/Helpers/Generate')
const WebPerformance = use('App/Helpers/WebPerformance')
const AperStats = use('App/Models/AperStats')
const CustomHomeCarouselImage = use('App/Models/CustomHomeCarouselImage')
const Store = use('App/Helpers/Store')
const _ = require('lodash')

const CarouselManagerCache = new Cache('redis')

class CarouselManager {
  constructor() {
    this.carousel = {}
  }

  static async categories(id) {
    const self = new this()
    let content = []
    let carousel = {}

    carousel = await self._info(id)
    carousel.content = 'categories'

    content = await self._categories(self.carousel.content)

    return {
      carousel,
      content,
      total_items: content.length,
    }
  }

  static async subcategories(id) {
    const self = new this()

    const [category, content] = await Promise.all([Category.find(id), self._subcategories(id)])

    const carousel = {
      id,
      type: 'Carousel',
      name: 'Subcategorías',
      content: 'subcategories',
      content_type: 'categories',
      link: await Generate.categoryLink(category),
    }

    return {
      carousel,
      content,
      total_items: content.length,
    }
  }

  static async images(id) {
    const self = new this()

    const carousel = await self._info(id)
    carousel.content = 'images'

    if (Store.hasCustomHome()) {
      const content = (await CustomHomeCarouselImage.query().where('item_id', '=', id).fetch()).toJSON()

      const images = self._getImagesFromS3(content, id, 'carousel')

      return {
        carousel,
        images,
      }
    }
    const content = await self._images(id)

    return {
      carousel,
      content,
      total_items: content.length,
    }
  }

  _getImagesFromS3(content, item, contentType) {
    content.forEach((value) => {
      value.image_link = `${WebPerformance.getImagePath(
        '/img/customhome'
      )}/${contentType}_${item}_${WebPerformance.getImageHash(value.date_img_update, value.id)}.jpg`
    })

    return content
  }

  async _subcategories(id) {
    // let ids_array = ids.split(',').map(value => parseInt(value))

    const categories = await Category.query()
      .setHidden(['description', 'slug'])
      .where('id_parent', id)
      .where('active', true)
      .fetch()

    return categories.toJSON()
  }

  static async all_active_carousels() {
    let content = []

    content = await CarouselManagerCache.getOrFetch('active_carousels', () => CarouselManager._all_active_carousels())

    return content
  }

  static async products(id) {
    const self = new this()
    let content = []
    let carousel = {}

    carousel = await self._info(id)
    carousel.content = 'products'

    if (self.carousel.type === 'Category' || self.carousel.type_content === 'Categoria') {
      if (self.carousel.name.toLowerCase() === 'tiendas oficiales') {
        return { message: 'carousel invalid or not exists' }
      }

      const category = await self._category(self.carousel.content)
      carousel.link = category.link
      content = category.products
    } else if (self.carousel.type_content === 'Producto' || self.carousel.type === 'Product') {
      content = await self._products(self.carousel.content)
    }

    content.forEach((value) => {
      delete value._images
      delete value.pivot
    })

    await CarouselManager.aperStatsBadge(content)

    return {
      carousel,
      content,
      total_items: content.length,
    }
  }

  static async productsRelated(id) {
    const self = new this()
    const name = Env.get('CAROUSEL_NAME_RELATED_PRODUCTS', 'Otros productos que pueden interesarte')

    const carousel = {
      id,
      type: 'Carousel',
      name,
      content: 'products',
    }

    const category = await self._category(id, 25)

    const { products } = category
    carousel.link = category.link

    products.forEach((value) => {
      delete value._images
      delete value.pivot
    })

    await CarouselManager.aperStatsBadge(products)

    return {
      carousel,
      content: products,
      total_items: products.length,
    }
  }

  static async shops(id) {
    const self = new this()

    const carousel = await self._info(id)
    carousel.content = 'shops'

    const { name, content } = self.carousel

    const { id: categoryId, link } = await Category.query().where('name', name).orWhere('id', content).first()

    carousel.link = link

    const shops = (await OfficialShop.query().where('main_id', categoryId).fetch()).rows

    if (shops === null) {
      return {}
    }

    return {
      carousel,
      content: shops,
      total_items: content.length,
    }
  }

  async _info(id) {
    try {
      this.carousel = await CarouselManagerCache.getOrFetch(`carousel_${id}_info`, () =>
        Store.hasCustomHome() ? CustomHomeCarousel.findOrFail(id) : Carousel.findOrFail(id)
      )

      return {
        id: this.carousel.id,
        type: this.carousel.type,
        order: this.carousel.position,
        name: this.carousel.name,
        content: this.carousel.content,
        slideBy: this.carousel.slideBy,
        link: this.carousel.link,
        banner: this.carousel.banner,
        banner_link: this.carousel.banner_link,
        position: this.carousel.position,
        display: this.carousel.display,
        autoplay: this.carousel.autoplay,
        pagination: this.carousel.pagination,
      }
    } catch (error) {
      throw new CarosuelNotExistException(error.message)
    }
  }

  async _categories(ids) {
    const idsArray = ids.split(',').map((value) => parseInt(value, 10))

    const categories = await Category.query()
      .setHidden(['description', 'slug'])
      .whereIn('id', idsArray)
      .orderByRaw(`FIELD (id, ${ids})`)
      .fetch()

    return categories.toJSON()
  }

  async _category(id, limit = 15) {
    const config = await CarouselManagerCache.getOrFetch(`category_${id}_criterion`, () =>
      CategoryCriteron.findBy('category_id', id)
    )

    let orderBy = null

    if (config && (config.criterion === 'destacados' || config.criterion === 'outstanding')) {
      orderBy = `outstanding DESC`
    }

    const category = await Category.query()
      .distinct()
      .where('id', id)
      .with('products', (builder) => {
        builder
          .with('price')
          .with('marks')
          .with('supplier')
          .with('_images')
          .where('quantity', '>', 0)
          .where('active', true)

        if (orderBy) {
          builder.orderByRaw(orderBy)
        }
      })
      .first()

    const data = category.toJSON()

    data.products = _.take(
      _.uniqBy(data.products, (e) => e.id),
      limit
    )

    for (const product of data.products) {
      product.images = _.uniqWith(
        product.images,
        (arrVal, othVal) =>
          arrVal.link === othVal.link &&
          arrVal.type === othVal.type &&
          arrVal.height === othVal.height &&
          arrVal.width === othVal.width
      )
    }

    return data
  }

  async _images(id) {
    const images = await Banner.query().setHidden(['carousel_id']).where('carousel_id', id).fetch()

    return images.toJSON()
  }

  async _products(ids) {
    const idsArray = ids.split(',').map((value) => parseInt(value, 10))

    const products = await Product.query()
      .distinct()
      .with('price')
      .with('marks')
      .with('supplier')
      .with('_images')
      .where('quantity', '>', 0)
      .where('active', true)
      .whereIn('id', idsArray)
      .orderByRaw(`FIELD (id, ${ids})`)
      .limit(15)
      .fetch()

    return products.toJSON()
  }

  async _all_active_carousels() {
    const carousels = await Carousel.query().where('active', 1).fetch()

    return carousels.toJSON()
  }

  static async aperStatsBadge(content) {
    console.log('************************DO aperStatsBadge()')
    console.log(content)
    const aperStats = new AperStats()
    try {
      console.log('LLEGA1')
      await aperStats.init()
      console.log('LLEGA2')

      await Promise.all(
        content.map(async (c) => {
          c.badge = await aperStats.getsBadge(c.supplier.id)
        })
      )
    } catch (ex) {
      console.error(ex)
    }
  }
}

module.exports = CarouselManager
