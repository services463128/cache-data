const Order = use('App/Models/Order')

class OrderManager {
  static async find(id) {
    const data = await Order.query()
      .with('state')
      .with('customer')
      .with('carrier')
      .with('address_delivery')
      .with('address_invoice')
      .with('payments', (builder) => {
        builder.with('supplier')
      })
      .with('items')
      .where('id', id)
      .first()

    return data.toJSON()
  }
}

module.exports = OrderManager
