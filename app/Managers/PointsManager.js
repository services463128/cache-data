const Cache = use('App/Helpers/Cache')
const PointPercentage = use('App/Models/Point/Percentage')
const PointTool = use('App/Models/Point/Tool')
const PointConversion = use('App/Models/Point/Conversion')
const Store = use('App/Helpers/Store')

const PointsManagerCache = new Cache('local')

class PointsManager {
  static points(price, product = null) {
    if (Store.getCurrent() === Store.BBVA) {
      return PointsManager._bbvaPoints(price)
    }
    return PointsManager._defaultPoints(price, product)
  }

  static async _bbvaPoints(price) {
    const points = []

    try {
      const percentages = await PointsManagerCache.getOrFetch(
        `point_percentage_actives`,
        () => PointPercentage.query().where('percentage', '>', 0).where('active', true).fetch(),
        60
      )

      const conversion = await PointsManagerCache.getOrFetch(
        `point_conversion_0100`,
        () => PointConversion.query().where('category', '0100').where('active', true).firstOrFail(),
        60
      )

      for (const row of percentages) {
        const newPrice = price.final * row.percentage
        const amount = newPrice / conversion.factor
        const cash = price.final - newPrice
        const percentage = row.percentage * 100

        points.push({
          percentage,
          amount: parseInt(amount, 10),
          cash: parseFloat(cash.toFixed(6)),
        })
      }
    } catch (error) {
      console.error(error)
    }

    return points
  }

  static async _defaultPoints(price, product = null) {
    const points = []

    try {
      const defaultPercentages = await PointsManagerCache.getOrFetch(
        `point_percentage_default`,
        async () =>
          (
            await PointTool.query()
              .where('value', '!=', '0')
              .where((builder) => {
                builder.where('id', '=', '1').orWhere('key', '=', 'available_points')
              })
              .fetch()
          ).toJSON(),
        60
      )

      const percentages = await PointsManagerCache.getOrFetch(
        `point_percentage_actives`,
        async () => {
          let query = PointPercentage.query().where('available_points', '!=', '0').where('active', true)
          if (product && product.supplier_id) {
            query = query.where('id_supplier', product.supplier_id)
          }
          return (await query.fetch()).toJSON()
        },
        60
      )

      const conversion = await PointsManagerCache.getOrFetch(
        `point_conversion_0100`,
        async () =>
          (await PointConversion.query().where('category', '0100').where('active', true).firstOrFail()).toJSON(),
        60
      )

      for (const row of percentages) {
        let availablePoints = row.available_points

        if (!availablePoints) {
          availablePoints = defaultPercentages[0].value
        }

        const arrayPercentages = JSON.parse(`[${availablePoints}]`)

        for (const [, percent] of arrayPercentages.entries()) {
          const newPrice = (price.final * parseInt(percent, 10)) / 100
          const priceProduct = newPrice
          let rest = price.final - newPrice
          let reedemPoints = priceProduct / conversion.factor
          let pointsProduct = Math.trunc(reedemPoints)
          let cash = rest
          let percentage = parseInt(percent, 10)

          if (percent === 1) {
            const priceOnePoint = 1 * conversion.factor
            rest = price.final - priceOnePoint
            reedemPoints = 1
            pointsProduct = reedemPoints
          }

          cash = rest
          percentage = parseInt(percent, 10)

          points.push({
            price: parseFloat(price.final.toFixed(2)),
            percentage,
            points: pointsProduct,
            cash: parseFloat(cash.toFixed(2)),
            factor: conversion.factor,
          })
        }
      }
    } catch (error) {
      console.log(error)
    }

    return points
  }
}

module.exports = PointsManager
