const Cache = use('App/Helpers/Cache')
const ImageType = use('App/Models/ImageType')
const Product = use('App/Models/Product')
const Generate = use('App/Helpers/Generate')
const _ = require('lodash')

const ProductManagerCache = new Cache('redis')

class ProductManager {
  static async find(id) {
    let data = {}

    try {
      // data = self.data = await Product.findOrFail(id)
      data = await Product.query()
        .with('price')
        .with('marks')
        .with('supplier')
        .with('category')
        .with('_images')
        .where('id', id)
        .first()
        .toJSON()

      delete data._images
      delete data.pivot
    } catch (error) {
      console.log(error)
    }

    return data
  }

  static async images(product) {
    const [images, types] = await Promise.all([
      ProductManagerCache.getOrFetch(`related_product_image_${product.id}`, async () =>
        (await product.getRelated('_images')).toJSON()
      ),
      ProductManagerCache.getOrFetch('entity_image_type', async () =>
        (await ImageType.query().where('products', '=', '1').fetch()).toJSON()
      ),
    ])

    const newImagesPromises = _.flatMap(images, (image) =>
      types.map(async (row) => ({
        // id: image.id,
        link: await Generate.imageLink({ id: image.id, type: row.name, slug: product.slug }),
        type: row.name,
        height: row.height,
        width: row.width,
      }))
    )

    return Promise.all(newImagesPromises)
  }
}

module.exports = ProductManager
