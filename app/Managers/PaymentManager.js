const Database = use('Database')
const Payment = use('App/Models/Payment')
const _ = require('lodash')

class PaymentManager {
  static async getFinancing({ cart_id: cartId }) {
    const payments = await Database.select('financing').from(Payment.table).where('cart_id', cartId)

    return _.sumBy(payments, (payment) => payment.financing)
  }
}

module.exports = PaymentManager
