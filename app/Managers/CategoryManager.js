const Category = use('App/Models/Category')

const DEFAULT_PAGE_NUMBER = 1
const DEFAULT_PAGE_SIZE = 10

class CategoryManager {
  static async products(params) {
    const parameters = params
    parameters.page = parameters.page ? parameters.page : DEFAULT_PAGE_NUMBER
    parameters.size = parameters.size ? parameters.size : DEFAULT_PAGE_SIZE
    parameters.product = parameters.product ? parseInt(parameters.product, 10) : ''
    parameters.from = parameters.from ? parameters.from : ''
    let data = {}

    const category = await Category.query()
      .distinct()
      .where('id', params.id)
      .with('products', (builder) => {
        builder
          .with('price')
          .with('marks')
          .with('supplier')
          .with('data_sheet')
          .with('attributes')
          .with('_images')
          .with('categories')
          .orderBy('id', 'asc')
          .forPage(params.page, params.size)

        if (params.product) {
          builder.where('v_products.id', params.product)
        }
        if (params.from) {
          builder.where('updated_at', '>', params.from)
        }
      })
      .first()

    if (category) {
      data = category.toJSON().products
    }

    return data
  }
}

module.exports = CategoryManager
