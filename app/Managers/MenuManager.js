const MenuManager16 = use('App/Managers/MenuManager16')
const MenuManager17 = use('App/Managers/MenuManager17')
const Store = use('App/Helpers/Store')

class MenuManager {
  static _getManager() {
    if (Store.isCurrentStore16()) {
      return MenuManager16
    }
    return MenuManager17
  }

  static generate() {
    return this._getManager().generate()
  }

  static generateItems() {
    return this._getManager().generateItems()
  }

  static generateCategories() {
    return this._getManager().generateCategories()
  }
}

module.exports = MenuManager
