/* eslint-disable no-param-reassign */
const Menu = use('App/Models/Menu')
const PHPUnserialize = use('php-unserialize')
const Category = use('App/Models/Category')

class MenuManager {
  constructor() {
    this.categories = {}
  }

  static async generate() {
    const [menus, categories] = await Promise.all([Menu.all(), Category.all()])

    this.categories = categories

    const data = []

    for (const value of menus.rows) {
      const item = {
        id: value['m.id_zmenu'],
        name: value.name,
        url: value.link,
        new_tab: value.link_newtab,
        sub: [],
      }

      if (value.content_type === 'category') {
        const categoryIds = PHPUnserialize.unserialize(value.categories)

        for (const categoryId of Object.values(categoryIds)) {
          const activeCategory = this.getActiveCategoryById(categoryId)
          if (activeCategory) {
            const category = this.formatCategory(activeCategory)

            const subs = this.getCategoryChilds(category.id)

            category.sub = subs.map((sub) => {
              const anotherSubs = this.getCategoryChilds(sub.id)
              sub.sub = anotherSubs.map((anotherSub) => this.formatCategory(anotherSub))
              return this.formatCategory(sub)
            })

            item.sub.push(category)
          }
        }
      }

      data.push(item)
    }

    return data
  }

  static formatCategory(data) {
    return {
      id: data.id,
      name: data.name,
      url: data.link,
      sub: data.sub ? data.sub : [],
    }
  }

  static getCategoryChilds(parentId) {
    return this.categories.rows.filter((category) => category.id_parent === parentId && category.active === 1)
  }

  static getActiveCategoryById(id) {
    return this.categories.rows.find((category) => category.id === parseInt(id, 10) && category.active === 1)
  }

  static async generateItems() {
    const list = await Menu.all()

    return list.rows.map((value) => ({
      id: value['m.id_zmenu'],
      name: value.name,
      url: value.link,
      new_tab: value.link_newtab,
      sub: [],
    }))
  }

  static async generateCategories() {
    const [menus, categories] = await Promise.all([
      Menu.query().where('content_type', 'LIKE', 'category').first(),
      Category.all(),
    ])

    this.categories = categories

    const categoryIds = PHPUnserialize.unserialize(menus.categories)
    const data = []

    for (const categoryId of Object.values(categoryIds)) {
      const activeCategory = this.getActiveCategoryById(categoryId)
      if (activeCategory) {
        const category = this.formatCategory(activeCategory)
        const subs = this.getCategoryChilds(category.id)

        category.sub = subs.map((sub) => {
          const anotherSubs = this.getCategoryChilds(sub.id)
          sub.sub = anotherSubs.map((anotherSub) => this.formatCategory(anotherSub))
          return this.formatCategory(sub)
        })

        data.push(category)
      }
    }

    return data
  }
}

module.exports = MenuManager
