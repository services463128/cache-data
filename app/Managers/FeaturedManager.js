const Cache = use('App/Helpers/Cache')
const CustomException = use('App/Exceptions/CustomException')
const FeaturedManagerCache = new Cache('redis')
const CustomHomeFeatured = use('App/Models/CustomHomeFeatured')
const CustomHomeItem = use('App/Models/CustomHomeItem')
const Product = use('App/Models/Product')

class FeaturedManager {
  static async products(id, limit) {
    const self = new this()

    let featured = await self._info(id)
    const item = await self._item(featured.item_id)
    const content = await self._products(featured.content, limit)

    featured = {
      id: featured.item_id,
      type: featured.content_type,
      order: item.position,
      name: item.title,
      content: 'products',
      display: 'full',
      autoplay: 0,
      pagination: 0,
    }

    return {
      featured,
      content,
      total_items: content.length,
    }
  }

  async _info(id) {
    const featured = await FeaturedManagerCache.getOrFetch(`featured_${id}_info`, () =>
      CustomHomeFeatured.findBy('item_id', id)
    )

    if (featured == null) {
      throw new CustomException(`item_id: ${id} is not found in ps_customhome_featured`, 404)
    }

    return featured
  }

  async _item(id) {
    const item = await FeaturedManagerCache.getOrFetch(`featured_${id}_item`, () => CustomHomeItem.findOrFail(id))

    if (item == null) {
      throw new CustomException(`id: ${id} item is not found`, 404)
    }

    return item
  }

  async _products(ids, limit) {
    const idsArray = ids.split(',').map((value) => parseInt(value, 10))

    if (!ids) {
      throw new CustomException(`id product is required`, 400)
    }

    const products = await Product.query()
      .distinct()
      .with('price')
      .with('marks')
      .with('supplier')
      .with('_images')
      .where('quantity', '>', 0)
      .where('active', true)
      .where('id_lang', 1)
      .whereIn('id', idsArray)
      .orderByRaw(`FIELD (id, ${ids})`)
      .limit(limit)
      .fetch()

    if (!products.toJSON().length) {
      throw new CustomException(`ids: ${ids} is not found`, 404)
    }

    return products.toJSON()
  }
}

module.exports = FeaturedManager
