const Cache = use('App/Helpers/Cache')
const Database = use('Database')
const Configuration = use('App/Models/Configuration')
const Product = use('App/Models/Product')
const BestPrice = use('App/Models/ProductBestPrice')

const BestPriceManagerCache = new Cache('local')

class BestPriceManager {
  static async find(productId, finalPrice) {
    const best = {
      retail: {
        amount: 0,
        percentage: 0,
      },
      exclusive: {
        amount: 0,
        percentage: 0,
      },
      exclusive_payroll: {
        amount: 0,
        percentage: 0,
      },
    }

    const global = {
      retail: {
        percentage: await BestPriceManagerCache.getOrFetch('PS_BESTPRICE_PORCENTAJE', async () => {
          const data = await Configuration.findBy('name', 'PS_BESTPRICE_PORCENTAJE')
          return data === null ? 0 : data.value
        }),
        refund_stop: await BestPriceManagerCache.getOrFetch('PS_BESTPRICE_TOPE_REINTEGRO', async () => {
          const data = await Configuration.findBy('name', 'PS_BESTPRICE_TOPE_REINTEGRO')
          return data === null ? 0 : data.value
        }),
      },
      exclusive: {
        percentage: await BestPriceManagerCache.getOrFetch('PS_BESTPRICE_PORCENTAJE_EXCLUSIVE', async () => {
          const data = await Configuration.findBy('name', 'PS_BESTPRICE_PORCENTAJE_EXCLUSIVE')
          return data === null ? 0 : data.value
        }),
        refund_stop: await BestPriceManagerCache.getOrFetch('PS_BESTPRICE_TOPE_REINTEGRO_EXCLUSIVE', async () => {
          const data = await Configuration.findBy('name', 'PS_BESTPRICE_TOPE_REINTEGRO_EXCLUSIVE')
          return data === null ? 0 : data.value
        }),
      },
      exclusive_payroll: {
        percentage: await BestPriceManagerCache.getOrFetch('PS_BESTPRICE_PORCENTAJE_EXCLUSIVE_PAYROLL', async () => {
          const data = await Configuration.findBy('name', 'PS_BESTPRICE_PORCENTAJE_EXCLUSIVE_PAYROLL')
          return data === null ? 0 : data.value
        }),
        refund_stop: await BestPriceManagerCache.getOrFetch(
          'PS_BESTPRICE_TOPE_REINTEGRO_EXCLUSIVE_PAYROLL',
          async () => {
            const data = await Configuration.findBy('name', 'PS_BESTPRICE_TOPE_REINTEGRO_EXCLUSIVE_PAYROLL')
            return data === null ? 0 : data.value
          }
        ),
      },
    }

    /*
     * NO USAR AQUI
     *
     * let product = await Product.findOrFail(product_id)
     *
     * Ya que ejecuta todos los HOOKs de Product lo cual generar overload.
     */

    const product = await Database.select('supplier_id').from(Product.table).where('id', productId).first()

    if (!!product.supplier_id === false) {
      return null
    }

    if (product.supplier_id < 1) {
      return null
    }

    const data = await BestPrice.findBy('supplier_id', product.supplier_id)

    for (const key of Object.keys(best)) {
      let percentage = 0
      let refundStop = 0

      if (data === null) {
        percentage = global[key].percentage
        refundStop = global[key].refund_stop
      } else {
        percentage = data[`percentage_${key}`]
        refundStop = data[`refund_stop_${key}`]
      }

      const priceDiff = (percentage * finalPrice) / 100

      if (priceDiff > refundStop) {
        best[key].amount = finalPrice - refundStop
      } else {
        best[key].amount = finalPrice - priceDiff
      }
      best[key].percentage = percentage
    }

    return best
  }
}

module.exports = BestPriceManager
