const Cache = use('App/Helpers/Cache')
const Currency = use('App/Models/Currency')
const BestPrice = use('App/Managers/BestPriceManager')
const getSymbolFromCurrency = require('currency-symbol-map')

const PriceManagerCache = new Cache('local')

class PriceManager {
  static list({ base, tax }) {
    const result = base + base * tax

    return parseFloat(result.toFixed(6))
  }

  static final({ list, reduction }) {
    const result = list - list * reduction

    return parseFloat(result.toFixed(6))
  }

  static async best({ product_id: productid, final }) {
    const result = await BestPrice.find(productid, final)
    const best = {}

    for (const index in result) {
      if (parseInt(result[index].percentage, 10) !== 0) {
        best[index] = {
          amount: parseFloat(result[index].amount.toFixed(6)),
          percentage: parseInt(result[index].percentage, 10),
        }
      }
    }

    if (!!best.exclusive_payroll === true) {
      best.default = best.exclusive_payroll
    } else if (!!best.exclusive === true) {
      best.default = best.exclusive
    } else if (!!best.retail === true) {
      best.default = best.retail
    }

    return best
  }

  static useQuotaFixedRates({ quota, quota_without_interest: quotaWithoutInterest }) {
    return {
      quota: quotaWithoutInterest < quota ? quota : 0,
      quota_without_interest: quotaWithoutInterest,
    }
  }

  static async getCurrency() {
    const signDefault = '$'

    const currency = await PriceManagerCache.getOrFetch(
      'DEFAULT_CURRENCY',
      async () => (await Currency.first()).toJSON(),
      24 * 60 * 60
    )
    const isoCode = currency.iso_code
    const sign = getSymbolFromCurrency(isoCode)

    const data = {
      isoCode,
      sign: sign || signDefault,
    }

    return data
  }
}

module.exports = PriceManager
