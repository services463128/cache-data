/* eslint-disable no-param-reassign */
const Menu = use('App/Models/Menu')
const Configuration = use('App/Models/Configuration')
const Cache = use('App/Helpers/Cache')
const OutstandSellerCategoryHelper = use('App/Helpers/OutstandSellerCategoryHelper')
const Env = use('Env')

const MenuManagerCache = new Cache('local')

class MenuManager {
  static async generate() {
    const [menuCategories, menuItems] = await Promise.all([Menu.all(), this._getMenuItems()])
    const categories = {}

    //console.log('****menuCategories',JSON.stringify(menuCategories,null,4))

    //console.log('****menuItems',JSON.stringify(menuItems,null,4))


    const item = menuItems.find((i) => i.includes('CAT'))

    //console.log('****item',JSON.stringify(item,null,4))

    //item="CAT2"
    if (item) {
      const idRootCategory = parseInt(item.match(/\d+$/)[0], 10)



      for (const [i, category] of menuCategories.rows.entries()) {
        const children = {}
        const id = category.id_category
        const parentId = category.id_parent

        //if(category.active == 1 || category.id_category == 2){
         // console.log('category2765---------')
         // console.log('category2765',JSON.stringify(category,null,4))

          if(category.available != 0) {
            children[id] = category

            if (id === idRootCategory) {
              categories[id] = category
            } else if (idRootCategory === parentId) { //solo si es del primer nivel de items...
            //} else {
              //console.log('EEEEEEEEEEEEEEEEEEEEEEEEEEEtra por aca categories[parentId].children=')
              categories[parentId].children = categories[parentId].children || {}
              categories[parentId].children[i] = children[id]
            }
          }  
        //}
      }
      //console.log('******************* antes del for *********************')

      for (const parent of Object.values(categories[idRootCategory].children)) {

        //console.log('parent',JSON.stringify(parent,null,4))


        this.getChilds(menuCategories, parent)
      }
    }
    //console.log('categories',JSON.stringify(categories,null,4))

    //console.log('categories["2765"]',JSON.stringify(categories["2765"],null,4))
    //console.log('categories[2765]',JSON.stringify(categories[2765],null,4))
    //console.log('categories[2765]',JSON.stringify(categories.2765,null,4))

    return categories
  }

  static async _getMenuItems() {
    const value = await MenuManagerCache.getOrFetch('MOD_BLOCKTOPMENU_ITEMS', async () => {
      const config = await Configuration.findBy('name', 'MOD_BLOCKTOPMENU_ITEMS')
      return config.value
    })

    return value ? value.split(',') : []
  }

  static getChilds(categories, parent) {
    
    const outstandSellersId = Env.get('CAT_OUTSTANDINGSELLERS_ID')
    //console.log('*******parent2=', JSON.stringify(parent,null,4))

    //console.log('outstandSellersId:',outstandSellersId)
    //console.log('parent.id_category:',parent.id_category)

    //console.log('*******categories=', JSON.stringify(categories,null,4))

  

    for (const [i, child] of Object.values(categories.rows).entries()) {

      //console.log('----------child.id_parent='+child.id_parent+' ==? '+outstandSellersId)


      if (outstandSellersId && child.id_parent == outstandSellersId) {

        //console.log('****************************ARMADO DE VENDEDORES DESTACADOS')


        child.link = OutstandSellerCategoryHelper.getLink(child.link)
      }

      if (child.id_parent === parent.id_category) {
        parent.children = parent.children || {}
        parent.children[i] = parent.children[i] || {}
        parent.children[i] = child
        this.getChilds(categories, child)
      }
    }
  }

  static generateItems() {
    throw new Error('Operation not implemented')
  }

  static generateCategories() {
    throw new Error('Operation not implemented')
  }
}

module.exports = MenuManager
