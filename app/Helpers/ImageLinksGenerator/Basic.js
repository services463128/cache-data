class BasicImageLinksGenerator {
  defaultImageExt

  constructor(defaultImageExt) {
    this.defaultImageExt = defaultImageExt
  }

  imageLink(baseUrl, id, type, slug) {
    if (id !== null) {
      return `${baseUrl}/${id}-${type}/${slug}.${this.defaultImageExt}`
    }
    return `${baseUrl}/img/p/es-default-${type}.${this.defaultImageExt}`
  }

  categoryBackground(baseUrl, id) {
    return `${baseUrl}/img/c/${id}_second-category_banner.${this.defaultImageExt}`
  }

  categoryImage(baseUrl, id) {
    return `${baseUrl}/img/c/${id}.${this.defaultImageExt}`
  }

  categoryImageMobile(baseUrl, id) {
    return `${baseUrl}/img/c/${id}.${this.defaultImageExt}`
  }

  categoryThumbImage(baseUrl, id) {
    return `${baseUrl}/img/c/${id}-0_thumb.${this.defaultImageExt}`
  }

  categoryThumbnailImage(baseUrl, id) {
    return `${baseUrl}/img/c/${id}_thumb.${this.defaultImageExt}`
  }

  markImage(baseUrl, name) {
    const filename = name.toLowerCase().replace(/ /g, '')

    const image = `${baseUrl}/img/productsmarks/${filename}.${this.defaultImageExt}`

    return image
  }

  officialShopImage(baseUrl, id) {
    return `${baseUrl}/img/c/${id}_thumb.${this.defaultImageExt}`
  }

  officialShopBackground(baseUrl, id) {
    return `${baseUrl}/img/c/${id}_second-category_banner.${this.defaultImageExt}`
  }
}

module.exports = BasicImageLinksGenerator
