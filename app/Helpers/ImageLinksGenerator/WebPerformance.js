const WebPerformance = use('App/Helpers/WebPerformance')

class WebPerformanceLinksGenerator {
  defaultImageExt

  constructor(defaultImageExt) {
    this.defaultImageExt = defaultImageExt
  }

  imageLink(baseUrl, id, type, slug, dateImageUpdate) {
    const path = WebPerformance.getImagePath('/img/p', baseUrl)

    if (id !== null) {
      const hash = WebPerformance.getImageHash(dateImageUpdate, '', type)
      return `${path}/${id}${hash}.${this.defaultImageExt}`
    }

    return `${path}/es-default-${type}.${this.defaultImageExt}`
  }

  categoryBackground(baseUrl, id, dateImageUpdate) {
    const imageTypeIdx = '1'
    const hash = WebPerformance.getImageHash(dateImageUpdate, imageTypeIdx)
    const path = WebPerformance.getImagePath('/img/c', baseUrl)

    return `${path}/${id}${hash}.${this.defaultImageExt}`
  }

  categoryImage(baseUrl, id, dateImageUpdate) {
    const imageTypeIdx = '1'
    const hash = WebPerformance.getImageHash(dateImageUpdate, imageTypeIdx)
    const path = WebPerformance.getImagePath('/img/c', baseUrl)

    return `${path}/${id}${hash}.${this.defaultImageExt}`
  }

  categoryImageMobile(baseUrl, id, dateImageUpdate) {
    const imageTypeIdx = '2'
    const hash = WebPerformance.getImageHash(dateImageUpdate, imageTypeIdx)
    const path = WebPerformance.getImagePath('/img/c', baseUrl)

    return `${path}/${id}${hash}.${this.defaultImageExt}`
  }

  categoryThumbImage(baseUrl, id, dateImageThumbUpdate) {
    const imageTypeIdx = '3'
    const hash = WebPerformance.getImageHash(dateImageThumbUpdate, imageTypeIdx)
    const path = WebPerformance.getImagePath('/img/c', baseUrl)

    return `${path}/${id}${hash}.${this.defaultImageExt}`
  }

  categoryThumbnailImage(baseUrl, id, dateImageThumbnailUpdate) {
    const imageTypeIdx = '4'
    const hash = WebPerformance.getImageHash(dateImageThumbnailUpdate, imageTypeIdx)
    const path = WebPerformance.getImagePath('/img/c', baseUrl)

    return `${path}/${id}${hash}.${this.defaultImageExt}`
  }

  markImage(baseUrl, name) {
    const filename = name.toLowerCase().replace(/ /g, '')
    const path = WebPerformance.getImagePath('/img/productsmarks', baseUrl)

    return `${path}/${filename}.${this.defaultImageExt}`
  }

  officialShopImage(baseUrl, id) {
    const path = WebPerformance.getImagePath('/img/c', baseUrl)

    return `${path}/${id}_thumb.${this.defaultImageExt}`
  }

  officialShopBackground(baseUrl, id) {
    const path = WebPerformance.getImagePath('/img/c', baseUrl)

    return `${path}/${id}_thumb.${this.defaultImageExt}`
  }
}

module.exports = WebPerformanceLinksGenerator
