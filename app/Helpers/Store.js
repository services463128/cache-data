const Env = use('Env')

class Store {
  static get VALYRIO() {
    return 'valyrio'
  }

  static get OMNI() {
    return 'omni'
  }

  static get TENGO() {
    return 'tengo'
  }

  static get GALICIA() {
    return 'galicia'
  }

  static get BANCOLOMBIA() {
    return 'bancolombia'
  }

  static get TIENDA_NARANJA() {
    return 'itaupy'
  }

  static get TIENDA_IUPP() {
    return 'itauar'
  }

  static get SUPERVIELLE() {
    return 'supervielle'
  }

  static get BBVA_MX() {
    return 'bbvamx'
  }

  static get BBVA() {
    return 'bbva'
  }

  static get TIENDA_CLIC() {
    return 'tclic'
  }

  static get ICBC() {
    return 'icbc'
  }

  static STORES_16 = [Store.ICBC, Store.TIENDA_CLIC, Store.BBVA, Store.SUPERVIELLE]

  static STORES_17 = [
    Store.VALYRIO,
    Store.GALICIA,
    Store.BANCOLOMBIA,
    Store.BBVA_MX,
    Store.TIENDA_IUPP,
    Store.TIENDA_NARANJA,
    Store.TENGO,
    Store.OMNI,
  ]

  static STORES_WITHOUT_CUSTOM_HOME = [Store.BBVA]

  static STORES_USE_OLD_NAME_CATEGORY_IMAGE = [Store.BANCOLOMBIA, Store.TENGO, Store.OMNI]

  static ALL = Store.STORES_16.concat(Store.STORES_17)

  static getCurrent() {
    const store = Env.get('STORE')
    if (!store) {
      throw new Error(`Missing STORE env variable, use: ${Store.ALL.join(', ')}`)
    }
    return store
  }

  static isCurrentStore16() {
    return Store.STORES_16.includes(Store.getCurrent())
  }

  static isCurrentStore17() {
    return Store.STORES_17.includes(Store.getCurrent())
  }

  static hasCustomHome() {
    return !Store.STORES_WITHOUT_CUSTOM_HOME.includes(Store.getCurrent())
  }

  static useOldNameCategoryImages() {
    return Store.STORES_USE_OLD_NAME_CATEGORY_IMAGE.includes(Store.getCurrent())
  }

  static getNameFieldsCategoryImages() {
    const fields = {
      image: 'image',
      imageTwo: 'image_two',
      imgThumb: 'img_thumb',
      imgThumbnail: 'img_thumbnail',
    }

    const oldFields = {
      image: 'image',
      imageTwo: 'image_two',
      imgThumb: 'menu_image',
      imgThumbnail: 'icon_image',
    }

    return this.useOldNameCategoryImages() ? oldFields : fields
  }
}

module.exports = Store
