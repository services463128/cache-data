class Function {
  static toFixedDown(number, digits) {
    const reg = new RegExp(`(\\d+\\.\\d{${digits}})(\\d)`)
    const num = number.toString().match(reg)

    return num ? parseFloat(num[1]) : number.valueOf()
  }

  /**
   * Formato dia-mes-año
   * @param {*} dateString
   * @returns
   */
  static convertDate(dateString) {
    let x = ''
    if (dateString !== '') {
      const p = dateString.split(/\D/g)
      x = `${[p[2], p[1], p[0]].join('-')}T00:00:00.000Z`
    }
    return x
  }
}

module.exports = Function
