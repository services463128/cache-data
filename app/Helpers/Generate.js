const Cache = use('App/Helpers/Cache')
const Configuration = use('App/Models/Configuration')
const BasicImageLinksGenerator = use('App/Helpers/ImageLinksGenerator/Basic')
const WebPerformanceLinksGenerator = use('App/Helpers/ImageLinksGenerator/WebPerformance')
const WebPerformance = use('App/Helpers/WebPerformance')
const Env = use('Env')

const GenerateCache = new Cache('redis')

class Generate {
  static imageLinksGenerator = Generate._getImageLinksGenerator()

  static domainBaseUrl = Generate._generateBaseUrl(false)

  static domainMediaBaseUrl = Generate._generateBaseUrl(true)

  static async _domain({ ssl }) {
    let name = 'PS_SHOP_DOMAIN'

    if (ssl) {
      name = 'PS_SHOP_DOMAIN_SSL'
    }

    const data = await Configuration.findByOrFail('name', name)
    return data.value
  }

  static async _domainMedia() {
    const name = 'PS_MEDIA_SERVER_1'

    const data = await Configuration.findByOrFail('name', name)
    return data.value
  }

  static async _hasMediaServer() {
    const name = 'PS_MEDIA_SERVERS'
    let media = false

    const data = await Configuration.findByOrFail('name', name)
    const value = parseInt(data.value, 10)

    if (value === 1) {
      media = true
    }

    return media
  }

  static async _hasSSL() {
    const name = 'PS_SSL_ENABLED'
    let ssl = false

    const data = await Configuration.findByOrFail('name', name)
    const value = parseInt(data.value, 10)

    if (value === 1) {
      ssl = true
    }

    return ssl
  }

  static _getImageLinksGenerator() {
    const defaultImageExt = Env.get('DEFAULT_IMG_EXT', 'jpg')

    if (WebPerformance.isEnabled) {
      return new WebPerformanceLinksGenerator(defaultImageExt)
    }

    return new BasicImageLinksGenerator(defaultImageExt)
  }

  static async _generateBaseUrl(media = false) {
    let domain = ''

    const [ssl, hasMediaServer] = await Promise.all([Generate._hasSSL(), Generate._hasMediaServer()])

    if (media && hasMediaServer) {
      domain = await Generate._domainMedia()
    } else {
      domain = await Generate._domain({ ssl })
    }

    let protocol = 'http'

    if (ssl) {
      protocol = 'https'
    }

    return `${protocol}://${domain}`
  }

  static async carouselBannerImage({ id }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return `${baseUrl}/img/homecarousel/banner_${id}.jpg`
  }

  /**
   * Build image links for products
   *
   * @param {Integer} id
   * @param {String} type
   * @param {String} slug
   * @param {Datetime} date_img_update
   *
   * @returns {String}
   */
  static async imageLink({ id, type, slug, date_img_update: dateImageUpdate }) {
    const baseUrl = await Generate.domainMediaBaseUrl
    return Generate.imageLinksGenerator.imageLink(baseUrl, id, type, slug, dateImageUpdate)
  }

  /**
   * Get main category image link
   * This image is used as a banner
   * insde the category's page
   *
   * @param {Integer} id
   * @param {Datetime} date_img_update
   *
   * @returns {String}
   */
  static async categoryBackground({ id, date_img_update: dateImageUpdate }) {
    const baseUrl = await Generate.domainMediaBaseUrl
    return Generate.imageLinksGenerator.categoryBackground(baseUrl, id, dateImageUpdate)
  }

  /**
   * Get category image link
   * This image is used at home page
   *
   * @param {Integer} id
   * @param {String} slug
   * @param {Datetime} date_img_two_update
   *
   * @returns {String}
   */
  static async categoryImage({ id, date_img_update: dateImageUpdate }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return Generate.imageLinksGenerator.categoryImage(baseUrl, id, dateImageUpdate)
  }

  /**
   * Get second category image link
   * This image is used at home page
   * in mobile
   *
   * @param {Integer} id
   * @param {Datetime} date_img_two_update
   *
   * @returns {String}
   */
  static async categoryImageMobile({ id, date_img_two_update: dateImageMobileUpdate }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return Generate.imageLinksGenerator.categoryImageMobile(baseUrl, id, dateImageMobileUpdate)
  }

  /**
   * Get Thumb category image link
   * This image is used on the main menu
   * and carousel of categories
   *
   * @param {Integer} id
   * @param {Datetime} date_img_thumb_update
   *
   * @returns {String}
   */
  static async categoryThumbImage({ id, date_img_thumb_update: dateImageUpdate }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return Generate.imageLinksGenerator.categoryThumbImage(baseUrl, id, dateImageUpdate)
  }

  /**
   * Get Thumbnail category image link
   * This image is used at home page
   * inside carousels
   *
   * @param {Integer} id
   * @param {Datetime} date_img_thumb_update
   *
   * @returns {String}
   */
  static async categoryThumbnailImage({ id, date_img_thumbnail_update: dateImageThumbnailUpdate }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return Generate.imageLinksGenerator.categoryThumbnailImage(baseUrl, id, dateImageThumbnailUpdate)
  }

  /**
   * Get category link
   *
   * @param {Integer} id
   * @param {String} slug
   *
   * @returns {String}
   */
  static async categoryLink({ id, slug }) {
    const name = 'TRAVELS_CATEGORY_LINK'
    let link = ''

    if (slug === 'turismo' || slug === 'viajes') {
      link = await GenerateCache.getOrFetch(name, async () => {
        const data = await Configuration.findBy('name', name)
        return data == null ? '/' : data.value
      })
    } else {
      const aux = slug.split('/')
      link = `/${id}-${slug}`

      if (typeof aux[1] !== 'undefined') link = `/${aux[1]}-${aux[0]}`
    }

    return link
  }

  /**
   * Get product mark icon link
   *
   * @param {String} name
   *
   * @returns {String}
   */
  static async markImage({ name }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return Generate.imageLinksGenerator.markImage(baseUrl, name)
  }

  /**
   * Get main image link from official store
   * (Tiendas Oficiales)
   *
   * @param {String} name
   * @param {String} slugh
   * @param {Datetime} date_img_update
   *
   * @returns {String}
   */
  static async officialShopImage({ id, date_img_update: dateImageUpdate }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return Generate.imageLinksGenerator.officialShopImage(baseUrl, id, dateImageUpdate)
  }

  /**
   * Get second image link from official store
   * (Tiendas Oficiales)
   *
   * @param {String} name
   * @param {String} slugh
   * @param {Datetime} date_img_two_update
   *
   * @returns {String}
   */
  static async officialShopBackground({ id, date_img_two_update: dateImageUpdate }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return Generate.imageLinksGenerator.officialShopBackground(baseUrl, id, dateImageUpdate)
  }

  /**
   * Get product's main image link
   *
   * @param {Product} product
   *
   * @returns {String}
   */
  static async productCover(product) {
    let id = null
    let hasCover = false
    let dateImageUpdate = null

    const images = await GenerateCache.getOrFetch(`related_product_image_${product.id}`, async () => {
      if (product.getRelated('_images') !== undefined) {
        return (await product.getRelated('_images')).toJSON()
      }

      return (await product._images().fetch()).toJSON()
    })

    for (const image of images) {
      if (parseInt(image.cover, 10) === 1) {
        hasCover = true
        id = image.id
        dateImageUpdate = image.date_img_update
        break
      }
    }

    if (!hasCover && images.length) {
      id = images[0].id
    }

    const params = {
      id,
      type: 'home_default',
      slug: product.slug,
      date_img_update: dateImageUpdate,
    }

    return Generate.imageLink(params)
  }

  /**
   * Get product's link
   *
   * @param {Product} product
   *
   * @returns {String}
   */
  static async productLink(product) {
    let category = null

    /*
     * Posible solución más optima
     * Si el producto es cargado CON Category se carga la relación
     * Si el producto es cargado SIN Category se busca la categoría y se elimina la referencia del mismo
     */
    if (product.getRelated('category') !== undefined) {
      category = product.getRelated('category')
    } else {
      // category = await Category.findOrFail(product.category_id)
      category = await product.category().fetch()
    }

    /*
     * Otra Posible solución
     * A diferencia de la solución anterior en esta siempre se fuerza la carga de la relacion
     * Aunque no haya sido solicitado desde la solicitud base.
     */
    /* if (product.getRelated('category') == undefined) {
     *    await product.load('category')
     *  }
     *  category = product.getRelated('category')
     */

     // console.log(product)


    return `/${category.slug}/${product.id}-${product.slug}.html`
  }

  /**
   * Get suppliers's main image link
   *
   * @param {Integer} id
   *
   * @returns {String}
   */
  static async supplierImage({ id }) {
    const baseUrl = await Generate.domainMediaBaseUrl

    return `${baseUrl}/img/su/${id}.jpg`
  }
}

module.exports = Generate
