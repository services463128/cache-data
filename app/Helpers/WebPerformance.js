const Env = use('Env')

const APP_ENV = Env.get('NODE_ENV', 'development')
const S3_DOMAIN = Env.get('S3_DOMAIN')

class WebPerformance {
  static isEnabled = typeof S3_DOMAIN === 'string' && S3_DOMAIN.length > 1

  /**
   * Buil file name based on date
   * and time update.
   *
   * @param {Datetime} date
   * @param {String} image_index_type
   * @param {Boolean} type
   *
   * @returns {String}
   */
  static getImageHash(date, imageIndexType = '', type = false) {
    if (date && date !== '0000-00-00 00:00:00') {
      const hash = Date.parse(date) / 1000

      if (APP_ENV === 'local') {
        return `${imageIndexType}${hash}`
      }

      return type ? `${imageIndexType}${hash}-${type}` : `${imageIndexType}${hash}`
    }
    return ''
  }

  /**
   * Buil image path depending
   * on recieved params and
   * enviroment variables.
   *
   * @param {String} path
   *
   * @returns {String}
   */
  static getImagePath(path) {
    const imageFolder = APP_ENV === 'local' ? '/images' : '/img'

    switch (path) {
      case '/img/c':
        return `${S3_DOMAIN}${imageFolder}/categories`
      case '/img/p':
        return `${S3_DOMAIN}${imageFolder}/products`
      case '/img/productsmarks':
        return `${S3_DOMAIN}${imageFolder}/productsmarks`
      case '/img/customhome':
        return `${S3_DOMAIN}${imageFolder}/customhome`
      default:
        return S3_DOMAIN
    }
  }
}

module.exports = WebPerformance
