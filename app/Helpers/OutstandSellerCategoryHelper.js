const url = require('url')

class OutstandSellerCategoryHelper {
  static getLink(catLink) {
    const parsedUrl = url.parse(catLink)
    const catPath = parsedUrl.pathname
    // Determinacion de indices de los separadores
    const IdSeparatorIndex = catPath.indexOf('-')
    const catIdSeparatorIndex = catPath.lastIndexOf('/')
    // Obtencion de elementos del link
    const catId = catPath.substring(catPath.lastIndexOf('/') + 1)
    const catName = catPath.substring(IdSeparatorIndex, catIdSeparatorIndex)

    // Creacion de elementos de la nueva url
    const path = `/${catId}${catName}`
    const port = parsedUrl.port === null ? '' : `:${parsedUrl.port}`
    const protocol = parsedUrl.protocol === null ? '' : `${parsedUrl.protocol}//`
    const host = parsedUrl.hostname

    return protocol + host + port + path
  }
}

module.exports = OutstandSellerCategoryHelper
