/* eslint-disable max-classes-per-file */

const RedisCache = use('Cache')
const Env = use('Env')
const NodeCache = require('node-cache')
const { Mutex } = require('async-mutex')

const DEFAULT_CACHE_TTL = Env.get('DEFAULT_CACHE_TTL', 15 * 60)

class RedisCacheEngine {
  async get(key) {
    return RedisCache.get(key)
  }

  async set(key, data, ttl) {
    // We need to divide by 60 becase adonis-cache works in minutes but we want to expose ttls in seconds
    return RedisCache.add(key, data, ttl / 60)
  }

  async flush() {
    return RedisCache.flush()
  }
}

class LocalCacheEngine {
  localCache = new NodeCache()

  async get(key) {
    return Promise.resolve(this.localCache.get(key))
  }

  async set(key, data, ttl) {
    return Promise.resolve(this.localCache.set(key, data, ttl))
  }

  async flush() {
    return Promise.resolve(this.localCache.flushAll())
  }
}

class Cache {
  locks = new Map()

  constructor(engine) {
    switch (engine) {
      case 'local':
        this.engine = new LocalCacheEngine()
        break
      case 'redis':
        this.engine = new RedisCacheEngine()
        break
      default:
        throw new Error(`Invalid cache engine: ${engine}`)
    }
  }

  _getLock(key) {
    let lock = this.locks.get(key)
    if (!lock) {
      lock = new Mutex()
      this.locks.set(key, lock)
    }
    return lock
  }

  /**
   * This method stores a key in the cache. It implements the https://en.wikipedia.org/wiki/Double-checked_locking
   * to avoid multiple resolutions of dataFn. The goal of this function is to avoid https://en.wikipedia.org/wiki/Cache_stampede
   *
   * @param {String} key
   * @param {Function} dataFn
   * @param {number} ttl
   *
   * @returns {any}
   */
  async getOrFetch(key, dataFn, ttl = DEFAULT_CACHE_TTL) {
    //let value = await this.engine.get(key)
    let value
    //if (!value) {
      const release = await this._getLock().acquire()
      try {
        //value = await this.engine.get(key)
        //if (!value) {
          value = await dataFn()
          //await this.engine.set(key, value, ttl)
        //}
      } finally {
        release()
      }
    //}
    return value
  }

  async flush() {
    return this.engine.flush()
  }
}

module.exports = Cache
