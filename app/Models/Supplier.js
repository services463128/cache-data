/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Supplier extends Model {
  static boot() {
    super.boot()

    this.addHook('afterFind', 'SupplierHook.addExtraFieldsFind')
    this.addHook('afterFetch', 'SupplierHook.addExtraFieldsFetch')
  }

  static get table() {
    return 'v_suppliers'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      // 'category_id',
    ]
  }
}

module.exports = Supplier
