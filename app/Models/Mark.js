/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Mark extends Model {
  static boot() {
    super.boot()

    this.addHook('afterFind', 'MarkHook.addExtraFieldsFind')
    this.addHook('afterFetch', 'MarkHook.addExtraFieldsFetch')
  }

  static get table() {
    return 'v_marks'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }
}

module.exports = Mark
