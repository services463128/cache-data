const Model = use('Model')

class OrderState extends Model {
  static get table() {
    return 'v_order_states'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }
}

module.exports = OrderState
