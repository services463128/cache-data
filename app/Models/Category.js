/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Category extends Model {
  static boot() {
    super.boot()

    this.addHook('afterFind', 'CategoryHook.addExtraFieldsFind')
    this.addHook('afterFetch', 'CategoryHook.addExtraFieldsFetch')
  }

  static get table() {
    return 'v_categories'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }

  products() {
    return this.belongsToMany('App/Models/Product', 'category_id')
      .withPivot(['position'])
      .pivotModel('App/Models/CategoryProduct')
  }
}

module.exports = Category
