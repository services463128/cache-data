/* eslint-disable no-param-reassign */
const Generate = use('App/Helpers/Generate')
const _ = require('lodash')

const Store = use('App/Helpers/Store')

const CategoryHook = {
  async addExtraFieldsFind(categoryInstance) {
    await CategoryHook._addExtraFields([categoryInstance])
  },
  async addExtraFieldsFetch(categoryInstances) {
    await CategoryHook._addExtraFields(categoryInstances)
  },
  async _addExtraFields(categoryInstances) {
    const fields = Store.getNameFieldsCategoryImages()

    const promises = _.flatMap(categoryInstances, (inst) => [
      Generate.categoryLink(inst).then((v) => {
        inst.link = v
      }),
      Generate.categoryImage(inst).then((v) => {
        inst[fields.image] = v
      }),
      Generate.categoryImageMobile(inst).then((v) => {
        inst[fields.imageTwo] = v
      }),
      Generate.categoryThumbImage(inst).then((v) => {
        inst[fields.imgThumb] = v
      }),
      Generate.categoryThumbnailImage(inst).then((v) => {
        inst[fields.imgThumbnail] = v
      }),
    ])

    await Promise.all(promises)
  },
}

module.exports = CategoryHook
