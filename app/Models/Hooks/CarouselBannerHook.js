/* eslint-disable no-param-reassign */
const Generate = use('App/Helpers/Generate')

const CarouselBannerHook = {
  async addExtraFieldsFind(bannerInstance) {
    await CarouselBannerHook._addExtraFields([bannerInstance])
  },
  async addExtraFieldsFetch(bannerInstances) {
    await CarouselBannerHook._addExtraFields(bannerInstances)
  },
  async _addExtraFields(bannerInstances) {
    const promises = bannerInstances.map(async (bannerInstance) => {
      bannerInstance.image = await Generate.carouselBannerImage(bannerInstance)
    })

    await Promise.all(promises)
  },
}

module.exports = CarouselBannerHook
