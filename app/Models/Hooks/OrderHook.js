/* eslint-disable no-param-reassign */
const Manager = use('App/Managers/PaymentManager')

const OrderHook = {
  async addExtraFieldsFind(orderInstance) {
    await OrderHook._addExtraFields([orderInstance])
  },
  async addExtraFieldsFetch(orderInstances) {
    await OrderHook._addExtraFields(orderInstances)
  },
  async _addExtraFields(orderInstances) {
    const promises = orderInstances.map(async (orderInstance) => {
      orderInstance.total_financing = await Manager.getFinancing(orderInstance)
    })

    await Promise.all(promises)
  },
}

module.exports = OrderHook
