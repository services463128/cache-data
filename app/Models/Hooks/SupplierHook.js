/* eslint-disable no-param-reassign */
const Generate = use('App/Helpers/Generate')

const SupplierHook = {
  async addExtraFieldsFind(supplierInstance) {
    await SupplierHook._addExtraFields([supplierInstance])
  },
  async addExtraFieldsFetch(supplierInstances) {
    await SupplierHook._addExtraFields(supplierInstances)
  },
  async _addExtraFields(supplierInstances) {
    const promises = supplierInstances.map(async (inst) => {
      inst.image = await Generate.supplierImage(inst)
    })

    await Promise.all(promises)
  },
}

module.exports = SupplierHook
