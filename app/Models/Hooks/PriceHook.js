/* eslint-disable no-param-reassign */
const Manager = use('App/Managers/PriceManager')
const Store = use('App/Helpers/Store')

const PriceHook = {
  addExtraFieldsFind(priceInstance) {
    PriceHook._addExtraFields([priceInstance])
  },
  addExtraFieldsFetch(priceInstances) {
    PriceHook._addExtraFields(priceInstances)
  },
  async _addExtraFields(priceInstances) {
    const promises = priceInstances.map(async (priceInstance) => {
      priceInstance.list = Manager.list(priceInstance)
      priceInstance.final = Manager.final(priceInstance)

      const { quota, quota_without_interest: quotaWithoutInterest } = Manager.useQuotaFixedRates(priceInstance)
      priceInstance.quota = quota
      priceInstance.quota_without_interest = quotaWithoutInterest

      if (Store.getCurrent() === Store.BBVA) {
        const best = await Manager.best(priceInstance)
        priceInstance.best = best
      }

      const currency = await Manager.getCurrency(priceInstance)
      priceInstance.currency = currency
    })

    await Promise.all(promises)
  },
}

module.exports = PriceHook
