/* eslint-disable no-param-reassign */
const Generate = use('App/Helpers/Generate')
const ProductManager = use('App/Managers/ProductManager')
const PointsManager = use('App/Managers/PointsManager')
const _ = require('lodash')

const ProductHook = {
  async addExtraFieldsFind(productInstance) {
    await ProductHook._addExtraFields([productInstance])
  },
  async addExtraFieldsFetch(productInstances) {
    await ProductHook._addExtraFields(productInstances)
  },
  async _addExtraFields(productInstances) {
    const promises = _.flatMap(productInstances, async (inst) => {
      const [link, cover] = await Promise.all([Generate.productLink(inst), Generate.productCover(inst)])

      inst.link = link
      inst.cover = cover

      if (inst.getRelated('_images')) {
        const images = await ProductManager.images(inst)
        inst.images = images
      } else {
        inst.images = []
      }

      if (inst.getRelated('price') !== undefined && inst.work_points) {
        if (inst.getRelated('price') == null) {
          console.warn('Price NULL: ', inst)
        }

        const points = await PointsManager.points(inst.getRelated('price'))
        inst.points = points
      }
    })

    await Promise.all(promises)
  },
}

module.exports = ProductHook
