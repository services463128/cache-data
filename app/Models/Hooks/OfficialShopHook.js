/* eslint-disable no-param-reassign */
const Category = use('App/Models/Category')
const Generate = use('App/Helpers/Generate')
const _ = require('lodash')

const OfficialShopHook = {
  async addExtraFieldsFind(officialShopInstance) {
    await OfficialShopHook._addExtraFields([officialShopInstance])
  },
  async addExtraFieldsFetch(officialShopInstances) {
    await OfficialShopHook._addExtraFields(officialShopInstances)
  },
  async _addExtraFields(officialShopInstances) {
    const promises = _.flatMap(officialShopInstances, async (inst) => {
      const category = await Category.findByOrFail('name', inst.name)

      if (inst.link === '') {
        inst.link = category.link
      }

      return [
        Generate.officialShopImage(category).then((v) => {
          inst.image = v
        }),
        Generate.officialShopBackground(category).then((v) => {
          inst.background = v
        }),
      ]
    })

    await Promise.all(promises)
  },
}

module.exports = OfficialShopHook
