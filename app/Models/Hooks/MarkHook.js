/* eslint-disable no-param-reassign */
const Generate = use('App/Helpers/Generate')

const MarkHook = {
  async addExtraFieldsFind(markInstance) {
    await MarkHook._addExtraFields([markInstance])
  },
  async addExtraFieldsFetch(markInstances) {
    await MarkHook._addExtraFields(markInstances)
  },
  async _addExtraFields(markInstances) {
    const promises = markInstances.map(async (inst) => {
      inst.image = await Generate.markImage(inst)
    })

    await Promise.all(promises)
  },
}

module.exports = MarkHook
