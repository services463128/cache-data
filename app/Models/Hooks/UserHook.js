/* eslint-disable no-param-reassign */

const Hash = use('Hash')
const { v4: uuidv4 } = require('uuid')

const UserHook = {
  async beforeSave(userInstance) {
    userInstance.id = uuidv4()
    if (userInstance.dirty.secret) {
      userInstance.secret = await Hash.make(userInstance.secret)
    }
    if (userInstance.dirty.password) {
      userInstance.password = await Hash.make(userInstance.password)
    }
  },
}

module.exports = UserHook
