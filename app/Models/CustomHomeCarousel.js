/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CustomHomeCarousel extends Model {
  static get table() {
    return 'v_custom_home_carousels'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }
}

module.exports = CustomHomeCarousel
