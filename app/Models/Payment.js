const Model = use('Model')

class Payment extends Model {
  static get table() {
    return 'v_payments'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      // 'supplier_id'
    ]
  }

  // This only applies to BBVA AR
  getQuotas(quotas) {
    if (quotas === 7) {
      return 12
    }

    if (quotas === 8) {
      return 18
    }

    return quotas
  }

  supplier() {
    return this.belongsTo('App/Models/Supplier', 'supplier_id')
  }
}

module.exports = Payment
