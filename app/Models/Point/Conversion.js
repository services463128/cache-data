/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Conversion extends Model {
  static get table() {
    return 'v_points_conversions'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }
}

module.exports = Conversion
