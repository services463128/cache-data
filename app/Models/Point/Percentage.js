/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Percentage extends Model {
  static get table() {
    return 'v_points_percentage'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }
}

module.exports = Percentage
