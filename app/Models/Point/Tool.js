/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Tool extends Model {
  static get table() {
    return 'v_points_tools'
  }
}

module.exports = Tool
