/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
  static boot() {
    super.boot()

    this.addHook('afterFind', 'ProductHook.addExtraFieldsFind')
    this.addHook('afterFetch', 'ProductHook.addExtraFieldsFetch')
  }

  static get table() {
    return 'v_products'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return ['category_id', 'stock_id', 'supplier_id', '_images']
  }

  _images() {
    return this.hasMany('App/Models/Image').setHidden([
      // 'cover',
      'legend',
      //   'position',
      'product_id',
    ])
  }

  category() {
    return this.belongsTo('App/Models/Category', 'category_id')
  }

  categories() {
    return this.belongsToMany('App/Models/Category', 'product_id').pivotModel('App/Models/CategoryProduct')
  }

  marks() {
    return this.belongsToMany('App/Models/Mark', 'product_id').pivotModel('App/Models/MarkProduct')
  }

  price() {
    return this.hasOne('App/Models/ProductPrice').setHidden(['product_id'])
  }

  supplier() {
    return this.belongsTo('App/Models/Supplier', 'supplier_id')
  }

  data_sheet() {
    return this.hasMany('App/Models/ProductFeature', 'id', 'id_product')
  }

  attributes() {
    return this.hasMany('App/Models/ProductAttribute', 'id', 'id_product')
  }
}

module.exports = Product
