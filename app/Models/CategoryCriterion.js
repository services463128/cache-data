/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CategoryCriterion extends Model {
  static get table() {
    return 'v_categories_criterion'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }
}

module.exports = CategoryCriterion
