/* eslint-disable no-param-reassign */
const Model = use('Model')

class User extends Model {
  static get connection() {
    return 'mysql_write'
  }

  static get table() {
    return 'users'
  }

  static boot() {
    super.boot()

    this.addHook('beforeSave', 'UserHook.beforeSave')
  }

  static get primaryKey() {
    return 'id'
  }

  static get incrementing() {
    return false
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token')
  }
}

module.exports = User
