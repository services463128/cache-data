/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')

const DB_MASTER = Env.get('DB_STORE_MASTER', null)

class CustomHomeFeatured extends Model {
  static get connection() {
    return 'mysql_read'
  }

  static get table() {
    return `${DB_MASTER}.ps_customhome_featured`
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }
}

module.exports = CustomHomeFeatured
