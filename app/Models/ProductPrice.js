/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductPrice extends Model {
  static boot() {
    super.boot()

    this.addHook('afterFind', 'PriceHook.addExtraFieldsFind')
    this.addHook('afterFetch', 'PriceHook.addExtraFieldsFetch')
  }

  static get table() {
    return 'v_products_prices'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }

  getBase(base) {
    return parseFloat(base.toFixed(6))
  }

  product() {
    return this.belongsTo('App/Models/Category', 'product_id')
  }
}

module.exports = ProductPrice
