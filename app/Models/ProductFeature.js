/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductFeature extends Model {
  static boot() {
    super.boot()
  }

  static get table() {
    return 'v_products_features'
  }

  static get primaryKey() {
    return 'id_product'
  }

  static get hidden() {
    return ['id_product', 'id_feature', 'id_feature_value']
  }
}

module.exports = ProductFeature
