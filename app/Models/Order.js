/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Order extends Model {
  static boot() {
    super.boot()

    this.addHook('afterFind', 'OrderHook.addExtraFieldsFind')
    this.addHook('afterFetch', 'OrderHook.addExtraFieldsFetch')
  }

  static get table() {
    return 'v_orders'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return ['state_id', 'carrier_id', 'customer_id', 'address_delivery_id', 'address_invoice_id']
  }

  address_delivery() {
    return this.hasOne('App/Models/Address', 'address_delivery_id', 'id').setHidden(['customer_id'])
  }

  address_invoice() {
    return this.hasOne('App/Models/Address', 'address_invoice_id', 'id').setHidden(['customer_id'])
  }

  carrier() {
    return this.belongsTo('App/Models/Carrier')
  }

  customer() {
    return this.belongsTo('App/Models/Customer')
  }

  items() {
    return this.hasMany('App/Models/OrderDetail').setHidden(['order_id'])
  }

  payments() {
    return this.hasMany('App/Models/Payment', 'cart_id', 'cart_id').setHidden([
      'cart_id',
      'supplier_id',
      'amount_with_financing',
      'amount_without_financing',
      'financing',
    ])
  }

  state() {
    return this.hasOne('App/Models/OrderState', 'state_id', 'id')
  }
}

module.exports = Order
