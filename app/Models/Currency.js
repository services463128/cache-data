/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')

const DB_MASTER = Env.get('DB_STORE_MASTER', null)

class Currency extends Model {
  static get connection() {
    return 'mysql_read'
  }

  static get table() {
    return `${DB_MASTER}.ps_currency`
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id_currency'
  }

  static get hidden() {
    return [
      //
    ]
  }
}

module.exports = Currency
