/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Image extends Model {
  static boot() {
    super.boot()
  }

  static get table() {
    return 'v_images'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }

  product() {
    return this.belongsTo('App/Models/Product')
  }
}

module.exports = Image
