/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductAttribute extends Model {
  static boot() {
    super.boot()
  }

  static get table() {
    return 'v_products_attributes'
  }

  static get primaryKey() {
    return 'id_product'
  }

  static get hidden() {
    return ['id_product', 'detalle']
  }
}

module.exports = ProductAttribute
