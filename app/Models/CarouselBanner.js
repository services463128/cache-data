/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CarouselBanner extends Model {
  static boot() {
    super.boot()

    this.addHook('afterFind', 'CarouselBannerHook.addExtraFieldsFind')
    this.addHook('afterFetch', 'CarouselBannerHook.addExtraFieldsFetch')
  }

  static get table() {
    return 'v_carousel_banners'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }
}

module.exports = CarouselBanner
