const Model = use('Model')

class OrderDetail extends Model {
  static get table() {
    return 'v_order_details'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }
}

module.exports = OrderDetail
