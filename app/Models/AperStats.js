const Configuration = use('App/Models/Configuration')
const url = require('url')
const http = require('http')
const https = require('https')
const NodeCache = require('node-cache')

const cache = new NodeCache()

const adapters = {
  'http:': http,
  'https:': https,
}

class AperStats {
  getUrlAdapter(inputUrl) {
    return adapters[url.parse(inputUrl).protocol]
  }

  async init() {
    try {
      const config = await Configuration.findBy('name', 'API_APER_STATS_URL')
      if (config && config.value) {
        this.endpoint = config.value
        this.badgeFeatureActive = await this.getBadgeFeatureActive()
        this.globals = await this.getGlobals()
      }
    } catch (error) {
      console.error(error)
    }
  }

  isInstalled() {
    return !!this.endpoint
  }

  async getBadgeFeatureActive() {
    return (await this.getData('/features?feature_name=badge'))[0].feature_active
  }

  getGlobals() {
    return this.getData('/globals')
  }

  async getData(path) {
    return new Promise((resolve, reject) => {
      const req = this.getUrlAdapter(this.endpoint).get(this.endpoint + path, (res) => {
        let responseBody = ''

        res.on('data', (chunk) => {
          responseBody += chunk
        })

        res.on('end', () => {
          resolve(JSON.parse(responseBody))
        })
      })

      req.on('error', (err) => {
        reject(err)
      })

      req.end()
    })
  }

  isGreen(idSupplier) {
    var out = this.globals.some((global) =>{
       return global.id_supplier === idSupplier && global.global_color === 'green'
      })
    

    return out
  }

  isActive(idSupplier) {
    var out = this.badgeFeatureActive && this.globals.find((g) => g.id_supplier === idSupplier).active_features 
    return out
  }

  async getsBadge(idSupplier) {
    const {value} = await Configuration.findBy('name', 'PRIMARY_STORE_COLOR')
    return {
      active: this.isInstalled ? this.isGreen(idSupplier) && this.isActive(idSupplier) : false,
      primary_color: value ? value : '#ccc',
    }
  }
}

module.exports = AperStats
