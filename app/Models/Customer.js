/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Customer extends Model {
  static get table() {
    return 'v_customers'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return [
      //
    ]
  }

  address_delivery() {
    return this.hasMany('App/Models/Address').setHidden(['customer_id'])
  }

  address_invoice() {
    return this.hasMany('App/Models/Address').setHidden(['customer_id'])
  }
}

module.exports = Customer
