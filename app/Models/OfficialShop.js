/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class OfficalShop extends Model {
  static boot() {
    super.boot()

    this.addHook('afterFind', 'OfficialShopHook.addExtraFieldsFind')
    this.addHook('afterFetch', 'OfficialShopHook.addExtraFieldsFetch')
  }

  static get table() {
    return 'v_official_shops'
  }

  static get incrementing() {
    return false
  }

  static get primaryKey() {
    return 'id'
  }

  static get hidden() {
    return ['main_id', 'parent_id', 'category_id']
  }
}

module.exports = OfficalShop
