/*
|--------------------------------------------------------------------------
| Redis Configuaration
|--------------------------------------------------------------------------
|
| Here we define the configuration for redis server. A single application
| can make use of multiple redis connections using the redis provider.
|
*/

const Env = use('Env')

const redisHosts = Env.get('REDIS_CLUSTER_HOSTS', null)
  .split(',')
  .map((_) => ({ host: _.trim() }))

console.log(`Connecting to redis cluster: ${JSON.stringify(redisHosts)}`)

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | connection
  |--------------------------------------------------------------------------
  |
  | Redis connection to be used by default.
  |
  */
  connection: Env.get('REDIS_CONNECTION', 'local'),

  /*
  |--------------------------------------------------------------------------
  | local connection config
  |--------------------------------------------------------------------------
  |
  | Configuration for a named connection.
  |
  */
  local: {
    host: Env.get('REDIS_LOCAL_HOST', '127.0.0.1'),
    port: Env.get('REDIS_LOCAL_PORT', 6379),
    db: Env.get('REDIS_LOCAL_DATABASE', 0),
    keyPrefix: '',
  },

  /*
  |--------------------------------------------------------------------------
  | cluster config
  |--------------------------------------------------------------------------
  |
  | Below is the configuration for the redis cluster.
  |
  */
  cluster: {
    clusters: redisHosts,
    options: {
      slotsRefreshTimeout: 3000,
      dnsLookup: (address, callback) => callback(null, address),
      redisOptions: {
        showFriendlyErrorStack: true,
        tls: {
          checkServerIdentity: () => undefined,
        },
        password: Env.get('REDIS_CLUSTER_PASSWORD', null),
      },
      scaleReads: 'slave',
    },
  },
}
