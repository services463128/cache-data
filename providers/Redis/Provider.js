const { ServiceProvider } = require('@adonisjs/fold')
const CustomRedisFactory = require('./RedisFactory')
const Redis = require('./Redis')

class RedisProvider extends ServiceProvider {
  register() {
    this.app.bind('Adonis/Addons/RedisFactory', () => CustomRedisFactory)

    this.app.singleton('Adonis/Addons/Redis', (app) => {
      const RedisFactory = app.use('Adonis/Addons/RedisFactory')
      const Config = app.use('Adonis/Src/Config')
      return new Redis(Config, RedisFactory)
    })

    this.app.alias('Adonis/Addons/Redis', 'Redis')
  }
}

module.exports = RedisProvider
