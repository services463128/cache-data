/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('apertest', async () => 'OK')

Route.get('orders/:id', 'OrderController.show').middleware('auth')

Route.get('products/:id', 'ProductController.show')

Route.get('categories/:id', 'CategoryController.show')

Route.get('carousel/:type/:id', 'CarouselController.show')

Route.get('featured/product/:id/:limit?', 'FeaturedController.show')

Route.get('cache/clear', 'CacheController.flush')

Route.get('menu', 'MenuController.index')

Route.get('menu/items', 'MenuController.items')

Route.get('menu/categories', 'MenuController.categories')

Route.get('external/carousel/:type/:id', 'CarouselController.show')

Route.get('external/carousel/getEndpoints', 'CarouselController.getEndpoints')

Route.group(() => {
  Route.post('oauth/token', 'TokenController.issueToken')
  Route.post('oauth/token/refresh', 'TokenController.issueToken')
}).middleware(['check:content-type'])

Route.get('/digital_segment/category/:id', 'DigitalSegmentController.show')

Route.get('/digital_segment/categories', 'DigitalSegmentController.all')
